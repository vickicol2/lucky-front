import React, { useState, useEffect, useContext } from 'react';
import './MyPetsDetails.scss';
import { useParams, Link } from "react-router-dom";
import PetData from '../../../shared/components/PetData/PetData';
import { apiLucky } from '../../../shared/services/apiLucky';


export default function MyPetsDetails() {
    const id = useParams().id;
    const [myPetDetails, setMyPetDetails] = useState();


    useEffect(() => {

        apiLucky.get(process.env.REACT_APP_BACK_URL + '/pets/' + id)
            .then(res => {
                const pet = res.data.pet[0];
                setMyPetDetails(pet);
                // console.log(pet);
            })
    }, []);



    return (
        <>
            {
                myPetDetails ? <div className="c-animals-details">
                    <div className="c-animals-details__img b-margin__bottom--big">
                        <Link to="/adoption" className="c-animals-details__arrow"><span className="lucky-back-arrow b-color-salmon b-color-salmon--big"></span></Link>
                        <img src={myPetDetails.img?  myPetDetails.img: '/img/petsDefault.jpg'} alt={myPetDetails.name} />

                        <div className="c-animals-details__header pt-4 pb-3 col-7 d-flex flex-row align-items-center justify-content-between">
                                <span className={myPetDetails.gender === 'Macho' ? "b-color-ocean lucky-" + myPetDetails.gender : "b-color-salmon lucky-" + myPetDetails.gender}></span>
                                <p>{myPetDetails.name}</p>
                        </div>
                    </div>

                    <div className="ml-3 mr-5">
                        <PetData title={'Especie'} content={myPetDetails.specie} />
                        <PetData title={'Fecha de nacimiento'} content={myPetDetails.dateBirth} />
                        <PetData title={'Sexo'} content={myPetDetails.gender} />
                        <PetData title={'Tamaño'} content={myPetDetails.size} />
                        <PetData title={'Peso (Kg)'} content={myPetDetails.weight} />
                    </div>



                    <div className="c-animals-details__history p-3 b-margin__top--medium col-11 col-md-8 ml-auto mr-auto">
                        <p className="b-color-ocean b-color-ocean--medium mt-4 mb-4">Observaciones</p>
                        <p>{myPetDetails.observations}</p>
                    </div>




                </div>
                    : ''
            }
        </>

    )
}
