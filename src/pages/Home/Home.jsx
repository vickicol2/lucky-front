import React, { useState, useEffect, useContext } from 'react';
import './Home.scss';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";
import Portait from '../../shared/components/Portait/Portait';
import Navbar from '../../shared/components/Navbar/Navbar';
import { Carousel } from 'primereact/carousel';
import MenuHome from '../../shared/components/MenuHome/MenuHome';
import { actualPageContext } from '../../shared/contexts/actualPageContext';
import { apiLucky } from '../../shared/services/apiLucky';

const contenidoSlide = [
    {
        icon: 'c-menu-home__icon lucky-pet-adoption',
        title: 'Estado de la adopción',
        body: "Revisa el proceso de tus adopciones en curso",
        redirect: '/state/list'
    },
    {
        icon: 'c-menu-home__icon lucky-sponsor',
        title: 'Apadrina',
        body: "",
        redirect: '/adoption'
    },
    {
        icon: 'c-menu-home__icon lucky-donate',
        title: 'Dona',
        body: "",
        redirect: '/adoption'
    },
]

export default function Home() {
    const [page, setPage] = useContext(actualPageContext);
    const [name, setName] = useState();
    const [token, setToken] = useState(false);

    useEffect(() => {
        setPage('home');

        // Quite un if principal de if localstorage token existe (engloba los dos if de abajo)

        if (localStorage.getItem('userType') === 'user') {
            apiLucky.get('/user/profile')
                .then(res => {
                    if (res.data.message !== "error") { setName(res.data.fullname); }
                })
        } else if (localStorage.getItem('userType') === 'shelter') {
            apiLucky.get('/shelter/profile/' + localStorage.getItem('id'))
                .then(res => {
                    if (res.data.message !== "error") { setName(res.data.shelter.name); }
                })
        }

    }, []);

    // contentBody vendrá del back!
    const contentBody = [
        {
            id: 0,
            title: '10 lugares para visitar con tu perro en Madrid',
            src: '/img/petfriendly.jpg',

        },
        {
            id: 1,
            title: '¿Sabes qué comen las iguanas?',
            src: '/img/iguana.jpg',
        },
        {
            id: 2,
            title: '10 Curiosidades sobre las cobayas',
            src: '/img/cobaya.jpg',
        }
    ]

    const templateSlide = (contenidoSlide) => {
        return <MenuHome contenidoSlide={contenidoSlide} />
    }



    return (
        <div className="c-home b-margin__bottom--big">
            <h2 className="c-home__title-name mb-5">¡Hola {name}!</h2>
            <div className="b-primereact-carousel">
                <Carousel value={contenidoSlide} itemTemplate={templateSlide} numVisible={1} numScroll={1} circular={true}></Carousel>
            </div>

            <div className="b-separator mt-3"></div>

            <h4 className="c-home__title-news mt-5">Novedades</h4>

            <Portait contentType={"news"} contentBody={contentBody} />

        </div>
    )
}
