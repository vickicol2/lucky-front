import React, { useState, useEffect, useContext } from 'react';
import { useForm } from 'react-hook-form';
import { apiLucky } from '../../shared/services/apiLucky';
import { Dialog } from 'primereact/dialog';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";
// Para llenar los select
import specieServices from '../../shared/services/specieServices.json';
import genderServices from '../../shared/services/genderServices.json';
import sizeServices from '../../shared/services/sizeServices.json';
import RadioButton from '../../shared/components/RadioButton/RadioButton';
import ModalSend from '../../shared/components/ModalSend/ModalSend';
import { actualPageContext } from '../../shared/contexts/actualPageContext';

const city = ['Madrid', 'Barcelona', 'Granada', 'Valencia', 'Sevilla'];

export default function PetsRegister() {
    const [page, setPage] = useContext(actualPageContext);
    const { register, handleSubmit, watch, errors } = useForm();
    const [signUp, setSignUp] = useState(false);

    const [valueRadioBtn, setValueRdioBtn] = useState(false);

    // Modal
    const [showModal, setShowModal] = useState(false);
    const [position, setPosition] = useState('center');

    const onHide = () => {
        setShowModal(false);
    };

    useEffect(() => {
        setPage('other');
    }, [])


    const onSubmit = formData => {
        formData.img = formData.img[0];

        const getFormData = object => Object.keys(object).reduce((formData, key) => {
            formData.append(key, object[key]);
            return formData;
        }, new FormData());

        if (!!localStorage.getItem('userType')) {
            apiLucky.post('/' + localStorage.getItem('userType') + '/pets', getFormData(formData))
                .then(res => {
                    console.log(res);

                    if (res.data.message !== "error") {
                        setSignUp(true);
                        setShowModal(true);
                        if (position) { setPosition(position); }

                    } else {
                        setSignUp(false);
                        setShowModal(true);
                        if (position) { setPosition(position); }
                    }
            
                })
        }
    }

    const onValueChange = (e) => {
        console.log(e.target.value);
        setValueRdioBtn(e.target.value);
    }

    return (
        <div className="c-pets-register">
            <h4 className="b-margin__top--medium b-margin__bottom--medium">Registra a tu nueva mascota</h4>

            <form className="c-my-pets-new d-flex flex-column pl-5 pr-5" onSubmit={handleSubmit(onSubmit)}>

                <label htmlFor="name" className="mb-4">Nombre de tu mascota</label>
                <input type="text" className="mb-5 b-input" name="name" id="name" ref={register({ required: true })} />

                {/* Sexo */}
                <div className="b-margin__top--small d-flex flex-row justify-content-between">
                    <label htmlFor="gender">Sexo</label>
                    <div className="d-flex flex-row justify-content-end">
                        <div>
                            <input type="radio" className="ml-5 b-color-pineGreen" name="gender" value="Hembra" ref={register({ required: true })} />
                            <span className="b-color-pineGreen">Hembra</span>
                        </div>
                        <div>
                            <input type="radio" className="ml-5 b-color-pineGreen" name="gender" value="Macho" ref={register({ required: true })} />
                            <span className="b-color-pineGreen">Macho</span>
                        </div>
                    </div>
                </div>

                {/* <Select por specie /> */}
                <div className="b-select mt-5">
                    <label className="b-select__label" htmlFor="specie">Especie</label>
                    <select name="specie" id="specie" className="b-select__content" ref={register({ required: true })}>
                        {
                            specieServices.map((item, index) => {
                                return <option key={index} value={item.icon} className="b-select__opcion">{item.name}</option>
                            })
                        }
                    </select>
                </div>

                <label className="mb-4" htmlFor="birth">Fecha de nacimiento</label>
                <input type="date" className="b-input" name="dateBirth" id="dateBirth" ref={register()} />

                {/* <Select por Tamaño /> */}
                <div className="b-select">
                    <label className="b-select__label" htmlFor="size">Tamaño</label>
                    <select name="size" id="size" className="b-select__content" ref={register({ required: true })}>
                        {
                            sizeServices.map((item, index) => {
                                return <option key={index} value={item.name} className="b-select__opcion">{item.name}</option>
                            })
                        }
                    </select>
                </div>

                <label htmlFor="weight">Peso</label>
                <input type="number" name="weight" id="weight" className="b-input mt-4" placeholder="Kg" ref={register({ required: true })} />

                {localStorage.getItem('userType') === 'user' && <>
                    <label htmlFor="observation">Observación</label>
                    <textarea className="b-input mt-5" name="observations" id="observations" placeholder="..." ref={register()}></textarea>
                </>
                }

                {localStorage.getItem('userType') === 'shelter' && <>
                    {/* <Select por ciudad /> */}
                    <div className="b-select">
                        <label htmlFor="city">Ciudad</label>
                        <select name="city" id="city" className="b-select__content" ref={register({ required: true })}>
                            {
                                city.map((item, index) => {
                                    return <option key={index} value={item} className="b-select__opcion">{item}</option>
                                })
                            }
                        </select>
                    </div>


                    <input type="text" className="d-none" name="statePet" id="statePet" value="Disponible" ref={register({ required: true })}/>


                    <label className="mt-5" htmlFor="history">Mi historia</label>
                    <textarea className="b-input mt-5" name="history" id="history" ref={register()}></textarea>

                    {/* Vacunado */}
                    <div className="b-margin__top--small d-flex flex-row justify-content-between">
                        <label htmlFor="gender">Vacunado</label>
                        <div className="d-flex flex-row justify-content-end">
                            <div>
                                <input type="radio" className="ml-5 b-color-pineGreen" name="vaccinated" value={true} ref={register({ required: true })} />
                                <span className="b-color-pineGreen">Sí</span>
                            </div>
                            <div>
                                <input type="radio" className="ml-5 b-color-pineGreen" name="vaccinated" value={false} ref={register({ required: true })} />
                                <span className="b-color-pineGreen">No</span>
                            </div>
                        </div>
                    </div>

                    {/* Desparasitado */}
                    <div className="b-margin__top--small d-flex flex-row justify-content-between">
                        <label htmlFor="dewormed">Desparasitado</label>
                        <div className="d-flex flex-row justify-content-end">
                            <div>
                                <input type="radio" className="ml-5 b-color-pineGreen" name="dewormed" value={true} ref={register({ required: true })} />
                                <span className="b-color-pineGreen">Sí</span>
                            </div>
                            <div>
                                <input type="radio" className="ml-5 b-color-pineGreen" name="dewormed" value={false} ref={register({ required: true })} />
                                <span className="b-color-pineGreen">No</span>
                            </div>
                        </div>
                    </div>


                    {/* Sano */}
                    <div className="b-margin__top--small d-flex flex-row justify-content-between">
                        <label htmlFor="healthy">Sano</label>
                        <div className="d-flex flex-row justify-content-end">
                            <div>
                                <input type="radio" className="ml-5 b-color-pineGreen" name="healthy" value={true} ref={register({ required: true })} />
                                <span className="b-color-pineGreen">Sí</span>
                            </div>
                            <div>
                                <input type="radio" className="ml-5 b-color-pineGreen" name="healthy" value={false} ref={register({ required: true })} />
                                <span className="b-color-pineGreen">No</span>
                            </div>
                        </div>
                    </div>


                    {/* Esterilizado */}
                    <div className="b-margin__top--small d-flex flex-row justify-content-between">
                        <label htmlFor="sterilized">Esterilizado</label>
                        <div className="d-flex flex-row justify-content-end">
                            <div>
                                <input type="radio" className="ml-5 b-color-pineGreen" name="sterilized" value={true} ref={register({ required: true })} />
                                <span className="b-color-pineGreen">Sí</span>
                            </div>
                            <div>
                                <input type="radio" className="ml-5 b-color-pineGreen" name="sterilized" value={false} ref={register({ required: true })} />
                                <span className="b-color-pineGreen">No</span>
                            </div>
                        </div>
                    </div>


                    {/* Identificado */}
                    <div className="b-margin__top--small d-flex flex-row justify-content-between">
                        <label htmlFor="identified">Identificado</label>
                        <div className="d-flex flex-row justify-content-end">
                            <div>
                                <input type="radio" className="ml-5 b-color-pineGreen" name="identified" value={true} ref={register({ required: true })} />
                                <span className="b-color-pineGreen">Sí</span>
                            </div>
                            <div>
                                <input type="radio" className="ml-5 b-color-pineGreen" name="identified" value={false} ref={register({ required: true })} />
                                <span className="b-color-pineGreen">No</span>
                            </div>
                        </div>
                    </div>


                    {/* Microchip */}
                    <div className="b-margin__top--small d-flex flex-row justify-content-between">
                        <label htmlFor="microchip">Microchip</label>
                        <div className="d-flex flex-row justify-content-end">
                            <div>
                                <input type="radio" className="ml-5 b-color-pineGreen" name="microchip" value={true} ref={register()} />
                                <span className="b-color-pineGreen">Sí</span>
                            </div>
                            <div>
                                <input type="radio" className="ml-5 b-color-pineGreen" name="microchip" value={false} ref={register()} />
                                <span className="b-color-pineGreen">No</span>
                            </div>
                        </div>
                    </div>

                    <label className="b-margin__top--medium" htmlFor="birth">Tienes que saber que</label>
                    <textarea className="b-input mt-5" name="observations" id="observations" ref={register()}></textarea>

                    <label htmlFor="birth">Requisitos de adopción</label>
                    <textarea className="b-input mt-5" name="requirements" id="requirements" ref={register()}></textarea>

                    <label htmlFor="birth">Tasa de adopción</label>
                    <input type="number" className="mb-5 mt-5 b-input" name="rate" id="rate" ref={register({ required: true })} />


                </>
                }

                <label htmlFor="img" className="mb-4 mt-5">Sube una imagen de tu mascota</label>
                <input type="file" className="mb-5 b-input" name="img" id="img" ref={register({ required: true })} />

                <div className="row justify-content-center m-0 mb-5 mt-5">
                    <Link to="/adoption" className="col-5 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark">Cancelar</Link>
                    <input type="submit" className="col-5 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light" value="Enviar" />
                </div>

            </form>

            {/* Modal */}
            <div className="b-primereact-dialog">
                <Dialog visible={showModal} onHide={() => onHide()}>
                    <ModalSend signUp={signUp} saved={'pet'} redirect={'/adopcion'}/>
                </Dialog>
            </div>

        </div>
    )
}
