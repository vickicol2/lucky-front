import React, { useEffect, useContext } from 'react';
import BtonArrow from '../../shared/components/BtnArrow/BtnArrow';
import Navbar from '../../shared/components/Navbar/Navbar';
import { actualPageContext } from '../../shared/contexts/actualPageContext';
import { isLoggedContext } from '../../shared/contexts/isLoggedContext';
import { useHistory } from 'react-router-dom';
import { apiLucky } from '../../shared/services/apiLucky';


export default function Plus() {
    const [isLogged, setIsLogged] = useContext(isLoggedContext);
    const [page, setPage] = useContext(actualPageContext);
    let history = useHistory();

    useEffect(() => {
        setPage('plus');


    }, []);

    const signOut = () => {
    
        apiLucky.post('/' + localStorage.getItem('userType') + '/logoutall', )
        .then(res => {
            localStorage.removeItem('token');
        })
        
        localStorage.clear();
        setIsLogged(false);
        history.push('getin');

    }

    return (
        <div className="c-plus">
            <div className="b-margin__top--big">
                {localStorage.getItem('userType') === 'user' && <BtonArrow icon={'lucky-dog-house'} text={'Asociaciones protectoras'} redirect={'/plus'} />}
                <BtonArrow icon={'lucky-events'} text={'Eventos'} redirect={'/plus'} />
                <BtonArrow icon={'lucky-blog'} text={'Curiosidades'} redirect={'/plus'} />
                <BtonArrow icon={'lucky-info'} text={'Ayuda'} redirect={'/plus'} />
                <BtonArrow icon={'lucky-config'} text={'Configuración'} redirect={'/plus'} />
            </div>

            <div className="b-margin__top--extrabig">
                <div onClick={() => signOut()}>
                    <BtonArrow icon={'lucky-logout'} text={'Cerrar sesión'} />
                </div>
            </div>

        </div>
    )
}
