import React, { useState, useEffect, useContext } from 'react';
import Slide from '../../shared/components/Slide/Slide';
import './OnBoarding.scss';
import { actualPageContext } from '../../shared/contexts/actualPageContext';

export default function OnBoarding() {
    const [slide, setSlide] = useState(false);
    const [page, setPage] = useContext(actualPageContext);

    useEffect(() => {
        setPage('other');

        setTimeout(() => {
          setSlide(true);
        }, 1000);
      }, []);

    return (
        <div>
            {!slide && <div className="c-onboarding">
                <div className="c-onboarding__img">
                    <img src="/img/logo.png" alt="Lucky" />
                </div>
                <h1 className="c-onboarding__title">LUCKY</h1>
            </div>
            }

            {slide && <Slide />}

        </div>
    )
}
