import React, { useState, useEffect, useContext } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, useParams, matchPath } from "react-router-dom";
import Portait from '../../../shared/components/Portait/Portait';
import { apiLucky } from '../../../shared/services/apiLucky';
import { actualPageContext } from '../../../shared/contexts/actualPageContext';

// Pets que resultan del filtrado

export default function Animals() {
    const [petsFilter, setPetsFilter] = useState();
    const [petsSearch, setPetsSearch] = useState([]);
    const [page, setPage] = useContext(actualPageContext);

    useEffect(() => {
        setPage('other');
        
        apiLucky.get('/shelter/pets/5f46caff3691bb599c0ec115')
            .then(res => {
                const pets = res.data.pets;

                let PetsCity = [];
                let PetsAge = [];
                let PetsSpecie = [];
                let PetsGender = [];
                let PetsSize = [];

                if (localStorage.getItem('filter')) {
                    const city = JSON.parse(localStorage.getItem('filter')).city;
                    const age = JSON.parse(localStorage.getItem('filter')).age;
                    const specie = JSON.parse(localStorage.getItem('filter')).specie;
                    const gender = JSON.parse(localStorage.getItem('filter')).gender;
                    const size = JSON.parse(localStorage.getItem('filter')).size;

                    
                    city === '' ? PetsCity=pets : PetsCity = pets.filter( item => item.city === city);
                    
                    age === '' ? PetsAge=PetsCity : PetsAge = PetsCity.filter(item => item.age === age);

                    if (specie.length > 0) {
                        specie.map(specie => {
                            PetsAge.map(item => {
                                if (item.specie === specie) {
                                    PetsSpecie.push(item);
                                }
                            })
                        });
                    } else {
                        PetsSpecie = PetsAge;
                    }


                    if (gender.length > 0) {
                        gender.map(gender => {
                            PetsSpecie.map(item => {
                                if (item.gender === gender) {
                                    PetsGender.push(item);
                                }
                            })
                        });
                    } else {
                        PetsGender = PetsSpecie;
                    }


                    if (size.length > 0) {
                        size.map(size => {
                            PetsGender.map(item => {
                                if (item.size === size) {
                                    PetsSize.push(item);
                                }
                            })
                        });
                    } else {
                        PetsSize = PetsGender;
                    }

                    setPetsFilter(PetsSize);
                    setPetsSearch(PetsSize);

                } else {
                    setPetsFilter(pets);
                    setPetsSearch(pets);
                }
            })
    }, [])

    const searchPet = (value) => {
        const result = petsFilter.filter(item => item.name.toLowerCase().includes(value.toLowerCase()) ||
                                                 item.specie.toLowerCase().includes(value.toLowerCase()) ||
                                                 item.gender.toLowerCase().includes(value.toLowerCase()) ||
                                                 item.city.toLowerCase().includes(value.toLowerCase()) ||
                                                 item.size.toLowerCase().includes(value.toLowerCase()));
        setPetsSearch(result);
    }

    return (
        <div className="c-animals">
            <div className="row b-margin__top--medium b-margin__bottom--big align-items-center justify-content-center">

                <Link to="/adoption" className="col-1 col-md-2 p-0 b-color-salmon b-color-salmon--big"><span className="lucky-back-arrow"></span></Link>

                {/* Buscar... */}
                <div className="col-8 col-md-7">
                    <input type="search" className="c-input-search__text" placeholder="Buscar" onChange={$event => { searchPet($event.target.value) }} />
                    <span className="lucky-search c-input-search__icon"></span>
                </div>

                <Link to="/filterpet" className="col-1 col-md-2 p-0 b-color-salmon b-color-salmon--big"><span className="lucky-filter"></span></Link>
            </div>
            {petsSearch ? (petsSearch.length > 0 ? <Portait contentType={"pets"} contentBody={petsSearch} /> : <h4 className="mt-5">No hay resultados para su búsqueda...</h4>) : ''}

        </div>
    )
}
