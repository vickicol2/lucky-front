import React, { useState, useEffect, useContext } from 'react';
import { useParams, Link } from "react-router-dom";
import { TabView, TabPanel } from 'primereact/tabview';
import PetData from '../../../../shared/components/PetData/PetData';
import { apiLucky } from '../../../../shared/services/apiLucky';
import { Dialog } from 'primereact/dialog';
import { useForm } from 'react-hook-form';
import ModalAdoption from '../../../../shared/components/ModalAdoption/ModalAdoption';
import statePetServices from '../../../../shared/services/statePetServices.json';
import { actualPageContext } from '../../../../shared/contexts/actualPageContext';
const dateFormat = require('dateformat');

export default function PetsDetails() {
    const id = useParams().id; // id de la mascota
    localStorage.setItem('idPet', id);
    const [page, setPage] = useContext(actualPageContext);
    const { register, handleSubmit, watch, errors } = useForm();
    const [activeIndex, setActiveIndex] = useState(0);
    const [petDetails, setPetDetails] = useState();
    const [newStatePet, setNewStatePet] = useState();

    // Modal de adoption (User)
    const [showModalAdoption, setShowModalAdoption] = useState(false);
    const [position, setPosition] = useState('center');

    const onHideAdoption = () => {
        setShowModalAdoption(false);
    };


    const onShowAdoption = () => {
        setShowModalAdoption(true);
        if (position) {
            setPosition(position);
        }
    };

    // Modal de Editar pet (Shelter)
    const [showModalEdit, setShowModalEdit] = useState(false);

    const onHideEdit = () => {
        setShowModalEdit(false);
        setNewStatePet(newStatePet);
    };


    const onShowEdit = () => {
        setShowModalEdit(true);
        if (position) {
            setPosition(position);
        }
    };

    useEffect(() => {
        setPage('other');

            apiLucky.get('/shelter/pet/' + id)
                .then(res => {
                    setPetDetails(res.data.pet);
                })
        }, []);

    const onSubmit = formData => {
        onHideEdit();


        apiLucky.put('/shelter/pets/' + id, formData)
            .then(res => {
                setNewStatePet(res.data.pet.statePet);
            })
    }


    return (
        <>
            {petDetails ? <div className="c-animals-details">
                <div className="c-animals-details__img mb-5">
                    <Link to="/adoption" className="c-animals-details__arrow"><span className="lucky-back-arrow b-color-salmon b-color-salmon--big"></span></Link>
                    <img src={petDetails.img ? petDetails.img : '/img/petsDefault.jpg'} alt={petDetails.name} />

                    <div className="c-animals-details__header pt-3 pb-3 col-7 d-flex flex-row align-items-center justify-content-between">
                        <div className="d-flex flex-row">
                            <span className={petDetails.gender === 'Macho' ? "b-color-ocean lucky-" + petDetails.gender : "b-color-salmon lucky-" + petDetails.gender}></span>
                            <div className="ml-3">
                                <p>{petDetails.name}</p>
                                <p>{petDetails.city}</p>
                            </div>
                        </div>

                        {newStatePet === "En proceso" && <div className='c-portait-adoption__status c-portait-adoption__status--process d-flex flex-row align-items-center'>
                            <p className="mr-3">{newStatePet}</p>
                            <span className="lucky-circle c-portait-adoption__span"></span>
                        </div>
                        }

                        {newStatePet === "Aceptado" && <div className='c-portait-adoption__status c-portait-adoption__status--complete d-flex flex-row align-items-center'>
                            <p className="mr-3">{newStatePet}</p>
                            <span className="lucky-circle c-portait-adoption__span"></span>
                        </div>
                        }

                        {newStatePet === "Rechazado" && <div className='c-portait-adoption__status c-portait-adoption__status--reject d-flex flex-row align-items-center'>
                            <p className="mr-3">{newStatePet}</p>
                            <span className="lucky-circle c-portait-adoption__span"></span>
                        </div>
                        }

                        {newStatePet === "Disponible" && <div className='c-portait-adoption__status c-portait-adoption__status--available d-flex flex-row align-items-center'>
                            <p className="mr-3">{newStatePet}</p>
                            <span className="lucky-circle c-portait-adoption__span"></span>
                        </div>
                        }

                    </div>
                </div>

                <TabView className="b-primereact-tabview" activeIndex={activeIndex} onTabChange={(e) => setActiveIndex(e.index)}>
                    <TabPanel header="Datos">
                        <PetData title={'Especie'} content={petDetails.specie} />
                        <PetData title={'Fecha de nacimiento'} content={dateFormat(petDetails.dateBirth, 'shortDate')} />
                        <PetData title={'Sexo'} content={petDetails.gender} />
                        <PetData title={'Tamaño'} content={petDetails.size} />
                        <PetData title={'Peso (Kg)'} content={petDetails.weight} />

                        <p className="b-color-ocean b-color-ocean--medium mt-5 mb-4">Personalidad</p>
                        <div className="d-flex flex-row flex-wrap">
                            <label htmlFor="" className="b-label-personality mr-3 mt-4">Amigable</label>
                            <label htmlFor="" className="b-label-personality mr-3 mt-4">Cariñoso</label>
                            <label htmlFor="" className="b-label-personality mr-3 mt-4">Juguetón</label>
                            <label htmlFor="" className="b-label-personality mr-3 mt-4">Bueno con los niños</label>
                        </div>

                        <div className="c-animals-details__history p-3 mt-5 mb-5">
                            <p className="b-color-ocean b-color-ocean--medium mt-4 mb-4">Historia</p>
                            <p>{petDetails.history}</p>
                        </div>

                        <div className="row justify-content-center m-0 mt-5">
                            <Link to="/adoption" className="col-3 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light">Apadrinar</Link>
                            <button className="col-3 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark" label="Show" icon="pi pi-info-circle" onClick={() => { onShowAdoption() }}>Adoptar</button>
                        </div>
                    </TabPanel>

                    <TabPanel header="Salud">
                        <PetData title={'Vacunado'} content={petDetails.vaccinated ? 'Sí' : 'No'} />
                        <PetData title={'Desparasitado'} content={petDetails.dewormed ? 'Sí' : 'No'} />
                        <PetData title={'Sano'} content={petDetails.healthy ? 'Sí' : 'No'} />
                        <PetData title={'Esterilizado'} content={petDetails.sterilized ? 'Sí' : 'No'} />
                        <PetData title={'Identificado'} content={petDetails.identified ? 'Sí' : 'No'} />
                        <PetData title={'Microchip'} content={petDetails.microchip ? 'Sí' : 'No'} />

                        <div className="c-animals-details__history p-3 mt-5 mb-5">
                            <p className="b-color-ocean b-color-ocean--medium mt-4 mb-4">Tienes que saber que</p>
                            <p>{petDetails.observations}</p>
                        </div>

                        <div className="row justify-content-center m-0 mt-5">
                            <Link to="/adoption" className="col-3 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light">Apadrinar</Link>
                            <button className="col-3 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark" label="Show" icon="pi pi-info-circle" onClick={() => { onShowAdoption() }}>Adoptar</button>
                        </div>
                    </TabPanel>

                    <TabPanel header="Adopción">
                        <div className="c-animals-details__history p-3 mb-5">
                            <p className="b-color-ocean b-color-ocean--medium mt-4 mb-4">Requisitos de adopción</p>
                            <p>{petDetails.requirements}</p>
                        </div>

                        <div className="c-animals-details__history p-3 mt-5 mb-5">
                            <p className="b-color-ocean b-color-ocean--medium mt-4 mb-4">Tasa de adopción <span className="lucky-info b-color-salmon b-color-salmon--big ml-3"></span></p>
                            <p>{petDetails.rate} €</p>
                        </div>

                        <div className="c-animals-details__history p-3 mt-5 mb-5">
                            <p className="b-color-ocean b-color-ocean--medium mt-4 mb-4">¿Se envía a otra ciudad?</p>
                            <p>{petDetails.shipping ? 'Si se envía a otra ciudad' : 'No se envía a otra ciudad'}</p>
                        </div>

                        <div className="row justify-content-center m-0 mt-5">
                            <Link to="/adoption" className="col-3 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light">Apadrinar</Link>
                            <button className="col-3 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark" label="Show" icon="pi pi-info-circle" onClick={() => { onShowAdoption() }}>Adoptar</button>
                        </div>
                    </TabPanel>
                </TabView>

            </div>
                : ''
            }







            {/* Modal de adopción  (User) */}
            <div className="b-primereact-dialog">
                <Dialog visible={showModalAdoption} onHide={() => onHideAdoption()}>
                    <ModalAdoption onClick={() => onHideAdoption()} />
                </Dialog>
            </div>





        </>

    )
}
