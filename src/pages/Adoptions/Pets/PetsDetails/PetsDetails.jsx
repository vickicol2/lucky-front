import React, { useState, useEffect, useContext } from 'react';
import './PetsDetails.scss';
import { useParams, Link } from "react-router-dom";
import { TabView, TabPanel } from 'primereact/tabview';
import PetData from '../../../../shared/components/PetData/PetData';
import { apiLucky } from '../../../../shared/services/apiLucky';
import { Dialog } from 'primereact/dialog';
import { useForm } from 'react-hook-form';
import ModalAdoption from '../../../../shared/components/ModalAdoption/ModalAdoption';
import statePetServices from '../../../../shared/services/statePetServices.json';
import { actualPageContext } from '../../../../shared/contexts/actualPageContext';
const dateFormat = require('dateformat');

export default function PetsDetails() {
    const id = useParams().id;
    const [page, setPage] = useContext(actualPageContext);
    const { register, handleSubmit, watch, errors } = useForm();
    const [activeIndex, setActiveIndex] = useState(0);
    const [petDetails, setPetDetails] = useState();
    const [newStatePet, setNewStatePet] = useState();
    const [responseEdit, setResponseEdit] = useState(true);

    // Modal de adoption (User)
    const [showModalAdoption, setShowModalAdoption] = useState(false);
    const [position, setPosition] = useState('center');

    const onHideAdoption = () => {
        setShowModalAdoption(false);
    };


    const onShowAdoption = () => {
        setShowModalAdoption(true);
        if (position) {
            setPosition(position);
        }
    };

    // Modal de Editar pet (Shelter)
    const [showModalEdit, setShowModalEdit] = useState(false);

    const onHideEdit = () => {
        setShowModalEdit(false);
        setNewStatePet(newStatePet);
    };


    const onShowEdit = () => {
        setShowModalEdit(true);
        if (position) {
            setPosition(position);
        }
    };

    useEffect(() => {
        setPage('other');
        // Quite el if principal del token

        if (localStorage.getItem('userType') === 'user') {
            apiLucky.get('/user/pets/' + id)
                .then(res => {
                    console.log(res.data.message);

                    if (res.data.message !== "error") {
                        const pet = res.data.pet;
                        setPetDetails(pet);
                        setNewStatePet(pet.statePet)
                    } else {
                        setPetDetails(false);
                        setNewStatePet(false);
                    }

                })
        } else if (localStorage.getItem('userType') === 'shelter') {
            apiLucky.get('/shelter/pet/' + id)
                .then(res => {
                    console.log(res.data.message);
                    
                    if (res.data.message !== "error") {
                        const pet = res.data.pet;
                        setPetDetails(pet);
                        setNewStatePet(pet.statePet)
                    } else {
                        setPetDetails(false);
                        setNewStatePet(false);
                    }

                })
        }

    }, []);

    const onSubmit = formData => {
        onHideEdit();


        apiLucky.put('/shelter/pets/' + id, formData)
            .then(res => {
                console.log(res.data.message);
                if (res.data.message !== "no") {
                    setNewStatePet(res.data.pet.statePet);
                    setResponseEdit(true);
                } else {
                    setResponseEdit(false);
                }
            })
    }


    return (
        <>
            {localStorage.getItem('userType') === 'shelter' && petDetails ? <div className="c-animals-details">
                <div className="c-animals-details__img mb-5">
                    <Link to="/adoption" className="c-animals-details__arrow"><span className="lucky-back-arrow b-color-salmon b-color-salmon--big"></span></Link>
                    <img src={petDetails.img ? petDetails.img : '/img/petsDefault.jpg'} alt={petDetails.name} />

                    <div className="c-animals-details__header pt-3 pb-3 col-7 d-flex flex-row align-items-center justify-content-between">
                        <div className="d-flex flex-row">
                            <span className={petDetails.gender === 'Macho' ? "b-color-ocean lucky-" + petDetails.gender : "b-color-salmon lucky-" + petDetails.gender}></span>
                            <div className="ml-3">
                                <p>{petDetails.name}</p>
                                <p>{petDetails.city}</p>
                            </div>
                        </div>

                        {newStatePet === "En proceso" && <div className='c-portait-adoption__status c-portait-adoption__status--process d-flex flex-row align-items-center'>
                            <p className="mr-3">{newStatePet}</p>
                            <span className="lucky-circle c-portait-adoption__span"></span>
                        </div>
                        }

                        {newStatePet === "Aceptado" && <div className='c-portait-adoption__status c-portait-adoption__status--complete d-flex flex-row align-items-center'>
                            <p className="mr-3">{newStatePet}</p>
                            <span className="lucky-circle c-portait-adoption__span"></span>
                        </div>
                        }

                        {newStatePet === "Rechazado" && <div className='c-portait-adoption__status c-portait-adoption__status--reject d-flex flex-row align-items-center'>
                            <p className="mr-3">{newStatePet}</p>
                            <span className="lucky-circle c-portait-adoption__span"></span>
                        </div>
                        }

                        {newStatePet === "Disponible" && <div className='c-portait-adoption__status c-portait-adoption__status--available d-flex flex-row align-items-center'>
                            <p className="mr-3">{newStatePet}</p>
                            <span className="lucky-circle c-portait-adoption__span"></span>
                        </div>
                        }

                    </div>
                </div>
                {!responseEdit && <p className="b-color-salmon ml-5 mr-5">Ha ocurrido un error y no hemos podido actualizar el estado de tu mascota. Inténtalo de nuevo!</p>}


                <TabView className="b-primereact-tabview" activeIndex={activeIndex} onTabChange={(e) => setActiveIndex(e.index)}>
                    <TabPanel header="Datos">
                        <PetData title={'Especie'} content={petDetails.specie} />
                        <PetData title={'Fecha de nacimiento'} content={dateFormat(petDetails.dateBirth, 'shortDate')} />
                        <PetData title={'Sexo'} content={petDetails.gender} />
                        <PetData title={'Tamaño'} content={petDetails.size} />
                        <PetData title={'Peso (Kg)'} content={petDetails.weight} />

                        <p className="b-color-ocean b-color-ocean--medium mt-5 mb-4">Personalidad</p>
                        <div className="d-flex flex-row flex-wrap">
                            <label htmlFor="" className="b-label-personality mr-3 mt-4">Amigable</label>
                            <label htmlFor="" className="b-label-personality mr-3 mt-4">Cariñoso</label>
                            <label htmlFor="" className="b-label-personality mr-3 mt-4">Juguetón</label>
                            <label htmlFor="" className="b-label-personality mr-3 mt-4">Bueno con los niños</label>
                        </div>

                        <div className="c-animals-details__history p-3 mt-5 mb-5">
                            <p className="b-color-ocean b-color-ocean--medium mt-4 mb-4">Historia</p>
                            <p>{petDetails.history}</p>
                        </div>

                        <div className="row justify-content-center m-0">
                            {/* <Link to="/adoption" className="col-3 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light">Apadrinar</Link> */}
                            <button onClick={() => onShowEdit()} className="b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark" label="Show" icon="pi pi-info-circle">Editar estado de adopción</button>
                        </div>
                    </TabPanel>

                    <TabPanel header="Salud">
                        <PetData title={'Vacunado'} content={petDetails.vaccinated ? 'Sí' : 'No'} />
                        <PetData title={'Desparasitado'} content={petDetails.dewormed ? 'Sí' : 'No'} />
                        <PetData title={'Sano'} content={petDetails.healthy ? 'Sí' : 'No'} />
                        <PetData title={'Esterilizado'} content={petDetails.sterilized ? 'Sí' : 'No'} />
                        <PetData title={'Identificado'} content={petDetails.identified ? 'Sí' : 'No'} />
                        <PetData title={'Microchip'} content={petDetails.microchip ? 'Sí' : 'No'} />

                        <div className="c-animals-details__history p-3 mt-5 mb-5">
                            <p className="b-color-ocean b-color-ocean--medium mt-4 mb-4">Tienes que saber que</p>
                            <p>{petDetails.observations}</p>
                        </div>

                        <div className="row justify-content-center m-0">
                            <button onClick={() => onShowEdit()} className="b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark" label="Show" icon="pi pi-info-circle">Editar estado de adopción</button>
                        </div>
                    </TabPanel>

                    <TabPanel header="Adopción">
                        <div className="c-animals-details__history p-3 mb-5">
                            <p className="b-color-ocean b-color-ocean--medium mt-4 mb-4">Requisitos de adopción</p>
                            <p>{petDetails.requirements}</p>
                        </div>

                        <div className="c-animals-details__history p-3 mt-5 mb-5">
                            <p className="b-color-ocean b-color-ocean--medium mt-4 mb-4">Tasa de adopción <span className="lucky-info b-color-salmon b-color-salmon--big ml-3"></span></p>
                            <p>{petDetails.rate} €</p>
                        </div>

                        <div className="c-animals-details__history p-3 mt-5 mb-5">
                            <p className="b-color-ocean b-color-ocean--medium mt-4 mb-4">¿Se envía a otra ciudad?</p>
                            <p>{petDetails.shipping ? 'Si se envía a otra ciudad' : 'No se envía a otra ciudad'}</p>
                        </div>

                        <div className="row justify-content-center m-0">
                            {/* <Link to="/adoption" className="col-3 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light">Apadrinar</Link> */}
                            <button onClick={() => onShowEdit()} className="b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark" label="Show" icon="pi pi-info-circle">Editar estado de adopción</button>
                        </div>
                    </TabPanel>
                </TabView>

            </div>
                : ''
            }

            {localStorage.getItem('userType') === 'user' && petDetails ? <div className="c-animals-details">
                <div className="c-animals-details__img b-margin__bottom--big">
                    <Link to="/adoption" className="c-animals-details__arrow"><span className="lucky-back-arrow b-color-salmon b-color-salmon--big"></span></Link>
                    <img src={petDetails.img ? petDetails.img : '/img/petsDefault.jpg'} alt={petDetails.name} />

                    <div className="c-animals-details__header pt-4 pb-3 col-7 d-flex flex-row align-items-center justify-content-between">
                        <span className={petDetails.gender === 'Macho' ? "b-color-ocean lucky-" + petDetails.gender : "b-color-salmon lucky-" + petDetails.gender}></span>
                        <p>{petDetails.name}</p>
                    </div>
                </div>

                <div className="ml-3 mr-5">
                    <PetData title={'Especie'} content={petDetails.specie} />
                    <PetData title={'Fecha de nacimiento'} content={dateFormat(petDetails.dateBirth, 'shortDate')} />
                    <PetData title={'Sexo'} content={petDetails.gender} />
                    <PetData title={'Tamaño'} content={petDetails.size} />
                    <PetData title={'Peso (Kg)'} content={petDetails.weight} />
                </div>


                <div className="c-animals-details__history p-3 b-margin__top--medium col-11 col-md-8 ml-auto mr-auto">
                    <p className="b-color-ocean b-color-ocean--medium mt-4 mb-4">Observaciones</p>
                    <p>{petDetails.observations}</p>
                </div>

                {/* <div className="row justify-content-center m-0 mt-5">
                    <Link to="/adoption" className="col-3 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light">Apadrinar</Link>
                    <button className="col-3 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark" label="Show" icon="pi pi-info-circle" onClick={() => { onShowAdoption() }}>Adoptar</button>
                </div> */}

            </div> : ''
            }

            {!petDetails && <div className="d-flex flex-column align-items-center b-margin__top--extrabig">
                <div className="d-flex flex-row align-items-center mb-5">
                    <Link to='/adoption' className="col-1 col-md-2 p-0 b-color-salmon b-color-salmon--big"><span className="lucky-back-arrow"></span></Link>
                    <h4>¡Lo siento! Esta mascota no existe</h4>
                </div>
                <img className="b-img b-img--big" src="/img/onboarding3.png" alt="Logo Lucky" />
            </div>
            }



            {/* Modal de adopción  (User)
            <div className="b-primereact-dialog">
                <Dialog visible={showModalAdoption} onHide={() => onHideAdoption()}>
                    <ModalAdoption onClick={() => onHideAdoption()} />
                </Dialog>
            </div> */}


            {/* Modal de Editar  (Shelter)*/}
            <div className="b-primereact-dialog">
                <Dialog visible={showModalEdit} onHide={() => onHideEdit()}>
                    <div className="c-pets-register b-modal-edit">

                        <form onSubmit={handleSubmit(onSubmit)}>

                            <div className="mt-5 d-flex flex-column mb-5 align-items-center">
                                <label className="ml-auto mr-auto b-select__label col-12 mb-5" htmlFor="statePet">Estado de adopción</label>
                                <select name="statePet" id="statePet" className="m-auto b-select__content b-select--medium" ref={register({ required: true })}>
                                    <option value="">...</option>
                                    {
                                        statePetServices.map((item, index) => {
                                            return <option key={index} value={item.name} className="b-select__opcion">{item.name}</option>
                                        })
                                    }
                                </select>
                            </div>

                            <div className="row justify-content-center m-0 mt-5">
                                <p onClick={() => onHideEdit()} className="col-3 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light">Cancelar</p>
                                <input type="submit" value="Aplicar" className="col-3 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark" />
                            </div>

                        </form>

                    </div>
                </Dialog>
            </div>


        </>

    )
}
