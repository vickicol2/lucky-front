import React, { useState, useEffect, useContext } from 'react';
import './Adoptions.scss';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";
import InputSearch from '../../shared/components/InputSearch/InputSearch';
import BtnArrow from '../../shared/components/BtnArrow/BtnArrow';
import Portait from '../../shared/components/Portait/Portait';
import Navbar from '../../shared/components/Navbar/Navbar';
import LinkIcon from '../../shared/components/LinkIcon/LinkIcon';
import { Carousel } from 'primereact/carousel';
import { apiLucky } from '../../shared/services/apiLucky';
import { actualPageContext } from '../../shared/contexts/actualPageContext';

export default function Adoptions() {
    const [page, setPage] = useContext(actualPageContext);
    const [idPets, setIdPets] = useState([]);
    const [petsAdoptions, setPetsAdoptions] = useState([]);
    const [petsSearch, setPetsSearch] = useState([]);
    const [petsUser, setPetsUser] = useState([]);

    useEffect(() => {
        setPage('adoption');

        if (localStorage.getItem('userType') === 'user') {

            apiLucky.get('/user/pets')
                .then(res => {
                    console.log(res.data.message);
                    if (res.data.message !== "error") { setPetsUser(res.data.pets); }
                })

            apiLucky.get('/shelter/pets/5f46caff3691bb599c0ec115')  // id de una shelter
                .then(res => {
                    if (res.data.message !== "error") {
                        setPetsAdoptions(res.data.pets);
                        setPetsSearch(res.data.pets);
                    }
                })

        } else if (localStorage.getItem('userType') === 'shelter') {

            apiLucky.get('/shelter/pets/' + localStorage.getItem('id'))
                .then(res => {
                    if (res.data.message !== "error") {
                        setPetsAdoptions(res.data.pets);
                        setPetsSearch(res.data.pets);
                    } else {
                        setPetsAdoptions(false);
                        setPetsSearch(false);
                    }
                })
        }

    }, [])




    const templateSlide = (petsAdoptions) => {
        return <LinkIcon petsAdoptions={petsAdoptions} />
    }

    const searchPet = (value) => {
        let result = petsAdoptions.filter(item => item.name.toLowerCase().includes(value.toLowerCase()) ||
            item.specie.toLowerCase().includes(value.toLowerCase()) ||
            item.gender.toLowerCase().includes(value.toLowerCase()) ||
            item.city.toLowerCase().includes(value.toLowerCase()) ||
            item.size.toLowerCase().includes(value.toLowerCase()));
        setPetsSearch(result);
    }

    return (
        <div className="c-adoption b-margin__bottom--big">
            <div className="b-margin__top--big">

                {/* Buscar... */}
                <div className="col-12 col-md-11 col-lg-8">
                    <input type="search" className="c-input-search__text" placeholder="Buscar" onChange={$event => { searchPet($event.target.value) }} />
                    <span className="lucky-search c-input-search__icon"></span>
                </div>
            </div>

            <div className="d-flex flex-row ml-5 mt-5 align-items-center">
                <h4>Mis mascotas</h4>
                <Link to="/register/pet"><span className="lucky-plus b-color-salmon b-color-salmon--big ml-3"></span></Link>
            </div>

            <p className="ml-5 mt-2 mb-5">Accede al perfil de tus mascotas</p>

            <div className="b-primereact-carousel">
                {localStorage.getItem('userType') === 'user' && petsUser && <Carousel value={petsUser} itemTemplate={templateSlide} numVisible={3} numScroll={1} circular={true}></Carousel>}
                {localStorage.getItem('userType') === 'shelter' && petsAdoptions && <Carousel value={petsAdoptions} itemTemplate={templateSlide} numVisible={3} numScroll={1} circular={true}></Carousel>}

            </div>

            <div className="b-separator mt-3 mb-5"></div>


            {localStorage.getItem('userType') === 'user' && <>
                <BtnArrow icon={''} text={'Estado de la adopción'} redirect={'/state/list'} />
                <div className="d-flex flex-row align-items-center justify-content-between ml-5 mb-5 mt-5">
                    <h4>Animales en adopción</h4>
                    <Link to="/filterpet"><span className="c-adoption__icon lucky-filter"></span></Link>
                </div> </>
            }


            {/* Mascotas en adopción */}
            {petsSearch ? (petsSearch.length > 0 ? <Portait contentType={"pets"} contentBody={petsSearch} /> : <h4 className="mt-5">No hay resultados para su búsqueda...</h4>)
                : <h4 className="mt-5">No tienes mascotas registradas...</h4>}

        </div>
    )
}
