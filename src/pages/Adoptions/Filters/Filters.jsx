import React, { useState, useEffect, useContext } from 'react';
import './Filters.scss';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import BtnIcon from '../../../shared/components/BtnIcon/BtnIcon';
import { actualPageContext } from '../../../shared/contexts/actualPageContext';


import specieServices from '../../../shared/services/specieServices.json';
import genderServices from '../../../shared/services/genderServices.json';
import sizeServices from '../../../shared/services/sizeServices.json';
import cityServices from '../../../shared/services/cityServices.json';
import ageServices from '../../../shared/services/ageServices.json';

export default function Filters() {
    const [page, setPage] = useContext(actualPageContext);
    const [specieSelected, setSpecieSelected] = useState(false);
    const [genderSelected, setGenderSelected] = useState(false);
    const [sizeSelected, setSizeSelected] = useState(false);
    const [citySelected, setCitySelected] = useState(false);
    const [ageSelected, setAgeSelected] = useState(false);
    const [whichCity, setWhichCity] = useState('');
    const [whichAge, setWhichAge] = useState('');
    

    const [btnSelected, setBtnSelected] = useState(false);


    useEffect(() => {
        setPage('other');

        let resultCity = '';
        let resultAge = '';

        if (localStorage.getItem('filter')) {
            const resultLocalStorage = JSON.parse(localStorage.getItem('filter'));
            const resultSpecie = resultLocalStorage.specie;
            const resultGender = resultLocalStorage.gender;
            const resultSize = resultLocalStorage.size;
            resultCity = resultLocalStorage.city;
            resultAge = resultLocalStorage.age;

            if (resultCity !== ''){
                setCitySelected(true);
                setWhichCity(resultCity);
            }

            if (resultAge !== ''){
                setAgeSelected(true);
                setWhichAge(resultAge);
            }


            resultSpecie.map(specie => {
                specieServices.map(item => {
                    if (item.icon === specie) {
                        item.selected = true;
                    }
                })
            })


            resultGender.map(gender => {
                genderServices.map(item => {
                    if (item.icon === gender) {
                        item.selected = true;
                    }
                })
            })

            resultSize.map(size => {
                sizeServices.map(item => {
                    if (item.name === size) {
                        item.selected = true;
                        setSizeSelected(true);
                    }
                })
            })

        }


        let hasSpecie = specieServices.findIndex(i => i.selected);
        let hasGender = genderServices.findIndex(i => i.selected);
        let hasSize = sizeServices.findIndex(i => i.selected);
        let city = resultCity !== '' ? true : false;
        let age = resultAge !== '' ? true : false;

        if (city || age || hasSpecie>=0 || hasGender>=0 || hasSize>=0){
            setBtnSelected(true);
        } else {
            setBtnSelected(false);
            localStorage.removeItem('filter');
        }
    }, [])


    const limpiarFilters = () => {
        specieServices.map(item => {
            item.selected = false;
        })

        genderServices.map(item => {
            item.selected = false;
        })

        sizeServices.map(item => {
            item.selected = false;
        })


        localStorage.removeItem('filter');
        setBtnSelected(false);
        setWhichCity(false);
        setWhichAge(false);
    }

    const onChangeCity = (e) => {
        const city = e.target.value !== '' ? true : false;
        setCitySelected(city);
        setWhichCity(false);

        let filterStorage = {
            city: e.target.value,
            age: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).age : "",
            specie: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).specie : [],
            gender: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).gender : [],
            size: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).size : []
        };

        localStorage.setItem('filter', JSON.stringify(filterStorage));

        let hasSpecie = specieServices.findIndex(i => i.selected);
        let hasGender = genderServices.findIndex(i => i.selected);
        let hasSize = sizeServices.findIndex(i => i.selected);

        if (city || ageSelected || hasSpecie>=0 || hasGender>=0 || hasSize>=0){
            setBtnSelected(true);
        } else {
            setBtnSelected(false);
            localStorage.removeItem('filter');
        }
    }

    const onChangeAge = (e) => {
        const age = e.target.value !== '' ? true : false;
        setAgeSelected(age);
        setWhichAge(false);

        let filterStorage = {
            city: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).city : "",
            age: e.target.value,
            specie: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).specie : [],
            gender: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).gender : [],
            size: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).size : []
        };

        localStorage.setItem('filter', JSON.stringify(filterStorage));

        let hasSpecie = specieServices.findIndex(i => i.selected);
        let hasGender = genderServices.findIndex(i => i.selected);
        let hasSize = sizeServices.findIndex(i => i.selected);

        if (citySelected || age || hasSpecie>=0 || hasGender>=0 || hasSize>=0){
            setBtnSelected(true);
        } else {
            setBtnSelected(false);
            localStorage.removeItem('filter');
        }
    }

    const btnSpecie = (item) => {
        setSpecieSelected(!specieSelected);

        specieServices[item.id].selected = !specieServices[item.id].selected;

        let arrSpecie = JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).specie : [];

        if (specieServices[item.id].selected) {
            arrSpecie.push(item.icon);
        } else {
            arrSpecie = arrSpecie.filter(value => value !== specieServices[item.id].icon);
        }


        let filterStorage = {
            city: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).city : '',
            age: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).age : '',
            specie: arrSpecie,
            gender: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).gender : [],
            size: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).size : []
        };

        localStorage.setItem('filter', JSON.stringify(filterStorage));

        let hasSpecie = specieServices.findIndex(i => i.selected);
        let hasGender = genderServices.findIndex(i => i.selected);
        let hasSize = sizeServices.findIndex(i => i.selected);

        if (citySelected || ageSelected || hasSpecie>=0 || hasGender>=0 || hasSize>=0){
            setBtnSelected(true);
        } else {
            setBtnSelected(false);
            localStorage.removeItem('filter');
        }
    }

    const btnGender = (item) => {
        setGenderSelected(!genderSelected);

        genderServices[item.id].selected = !genderServices[item.id].selected;

        let arrGender = JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).gender : [];

        if (genderServices[item.id].selected) {
            arrGender.push(item.icon);
        } else {
            arrGender = arrGender.filter(value => value !== genderServices[item.id].icon);
        }


        let filterStorage = {
            city: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).city : '',
            age: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).age : '',
            specie: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).specie : [],
            gender: arrGender,
            size: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).size : []
        };

        localStorage.setItem('filter', JSON.stringify(filterStorage));

        let hasSpecie = specieServices.findIndex(i => i.selected);
        let hasGender = genderServices.findIndex(i => i.selected);
        let hasSize = sizeServices.findIndex(i => i.selected);

        if (citySelected || ageSelected || hasSpecie>=0 || hasGender>=0 || hasSize>=0){
            setBtnSelected(true);
        } else {
            setBtnSelected(false);
            localStorage.removeItem('filter');
        }
    }

    const btnSize = (item) => {

        setSizeSelected(!sizeSelected);

        sizeServices[item.id].selected = !sizeServices[item.id].selected;

        let arrSize = JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).size : [];


        if (sizeServices[item.id].selected) {
            arrSize.push(item.name);
        } else {
            arrSize = arrSize.filter(value => value !== sizeServices[item.id].name);
        }


        let filterStorage = {
            city: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).city : '',
            age: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).age : '',
            specie: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).specie : [],
            gender: JSON.parse(localStorage.getItem('filter')) ? JSON.parse(localStorage.getItem('filter')).gender : [],
            size: arrSize,
        };

        localStorage.setItem('filter', JSON.stringify(filterStorage));

        let hasSpecie = specieServices.findIndex(i => i.selected);
        let hasGender = genderServices.findIndex(i => i.selected);
        let hasSize = sizeServices.findIndex(i => i.selected);

        if (citySelected || ageSelected || hasSpecie>=0 || hasGender>=0 || hasSize>=0){
            setBtnSelected(true);
        } else {
            setBtnSelected(false);
            localStorage.removeItem('filter');
        }
    }

    return (
        <div className="c-filter">
            <div className="row m-0 mt-5 mb-5 d-flex flex-row justify-content-center align-items-center">
                <h2 className="mr-5 col-7">Filtros</h2>
                <Link className="ml-5 lucky-close" to="/adoption"></Link>
            </div>

            {/* <Select por ciudad /> */}
            <div className="b-select">
                <label className="b-select__label" htmlFor="city">Ciudad</label>
                <p className="mt-3">{whichCity}</p>
                <select name="city" id="city" className="b-select__content" onChange={($e) => onChangeCity($e)}>
                    <option value=""></option>
                    { cityServices.map((item, index) => {
                            return <option key={index} value={item.name} className="b-select__opcion">{item.name}</option>
                        })
                    }
                </select>
            </div>

            {/* Botones de especies */}
            <label htmlFor="animalBtns" className="c-filter__label">Especie</label>
            <div className="c-filter__buttons" id="animalBtns">
                {
                    specieServices.map((item, index) => {
                        return <BtnIcon key={index} onClick={() => btnSpecie(item)} selected={specieSelected} item={item} />
                    })
                }
            </div>


            {/* <Select por edad /> */}
            <div className="b-select">
                <label className="b-select__label" htmlFor="select">Edad</label>
                <p className="mt-3">{whichAge}</p>
                <select name="select" id="" className="b-select__content" onChange={($e) => onChangeAge($e)}>
                    <option value=""></option>
                    { ageServices.map((item, index) => {
                            return <option key={index} value={item.name} className="b-select__opcion">{item.name}</option>
                        })
                    }
                </select>
            </div>

            {/* Botones de género */}
            <label htmlFor="genderBtns">Sexo</label>
            <div className="c-filter__buttons" id="genderBtns">
                {
                    genderServices.map((item, index) => {
                        return <BtnIcon key={index} onClick={() => btnGender(item)} selected={genderSelected} item={item}/>

                    })
                }
            </div>


            {/* Botones de tamaño */}
            <label htmlFor="sizeBtns">Tamaño</label>
            <div className="c-filter__buttons" id="sizeBtns">
                {
                    sizeServices.map((item, index) => {
                        return <BtnIcon key={index} onClick={() => btnSize(item)} selected={sizeSelected} item={item}/>

                    })
                }
            </div>

            {/* Muestra el botón de aplicar si no hay elementos (btonIcons) seleccionados */}
            {!btnSelected && <div className="row justify-content-center m-0 mb-5">
                <Link to="/filterpet/result" className="col-11 b-btn-rectangle b-btn-rectangle--big b-btn-rectangle--salmon-dark" >Aplicar</Link>
            </div>
            }

            {/* Muestra el botón de aplicar y borrar cuando hay por lo menos un elemento (btonIcons) seleccionado */}
            {btnSelected && <div className="row justify-content-center m-0 mb-5">
                <button className="col-5 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light" onClick={() => limpiarFilters()}>Borrar filtros</button>
                <Link to="/filterpet/result" className="col-5 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark">Aplicar</Link>
            </div>
            }
        </div>
    )
}
