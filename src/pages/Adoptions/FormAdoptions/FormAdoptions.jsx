import React, { useState, useEffect, useContext } from 'react';
import { useForm } from 'react-hook-form';
import { apiLucky } from '../../../shared/services/apiLucky';
import { ProgressBar } from 'primereact/progressbar';
import { Link } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import ModalSend from '../../../shared/components/ModalSend/ModalSend';
import { actualPageContext } from '../../../shared/contexts/actualPageContext';


export default function FormAdoptions() {
    const [page, setPage] = useContext(actualPageContext);
    const [signUp, setSignUp] = useState(false);
    const { register, handleSubmit, watch, errors } = useForm();
    const [pageForm, setPageForm] = useState();
    const [acceptTerms, setAcceptTerms] = useState(false);
    const [fullName, setFullName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [dni, setDni] = useState('');
    const [address, setAddress] = useState('');
    const [postalCode, setPostalCode] = useState('');
    const [city, setCity] = useState('');


    const [haveOtherAnimals, setHaveOtherAnimals] = useState('');
    const [whichOtherAnimals, setWhichOtherAnimals] = useState('');
    const [wellWithOtherAnimals, setWellWithOtherAnimals] = useState('');
    const [whyChosenAdopt, setWhyChosenAdopt] = useState('');
    const [knowNeedsAnimal, setKnowNeedsAnimal] = useState('');
    const [knowHisExpenses, setKnowHisExpenses] = useState('');
    const [knowHisDiet, setKnowHisDiet] = useState('');

    const [typeHouse, setTypeHouse] = useState('');
    const [liveRenting, setLiveRenting] = useState('');
    const [homeAllowsAnimals, setHomeAllowsAnimals] = useState('');
    const [moveSoon, setMoveSoon] = useState('');
    const [hasGarden, setHasGarden] = useState('');
    const [liveOtherPeople, setLiveOtherPeople] = useState('');
    const [peopleAgreeAdoption, setPeopleAgreeAdoption] = useState('');
    const [agreeVisitHome, setAgreeVisitHome] = useState('');

    // Modal
    const [showModal, setShowModal] = useState(false);
    const [position, setPosition] = useState('center');

    const onHide = () => {
        setShowModal(false);
    };

    useEffect(() => {
        setPage('other');

        if (localStorage.getItem('form1') || localStorage.getItem('form2') || localStorage.getItem('form3')) {
            if (localStorage.getItem('form1')) {
                setPageForm(1);
                setFullName(JSON.parse(localStorage.getItem('form1')).fullName);
                setEmail(JSON.parse(localStorage.getItem('form1')).email);
                setPhone(JSON.parse(localStorage.getItem('form1')).phone);
                setDni(JSON.parse(localStorage.getItem('form1')).dni);
                setAddress(JSON.parse(localStorage.getItem('form1')).address);
                setPostalCode(JSON.parse(localStorage.getItem('form1')).postalCode);
                setCity(JSON.parse(localStorage.getItem('form1')).city);
            }

            if (localStorage.getItem('form2')) {
                setPageForm(2);
                setHaveOtherAnimals(JSON.parse(localStorage.getItem('form2')).haveOtherAnimals);
                setWhichOtherAnimals(JSON.parse(localStorage.getItem('form2')).whichOtherAnimals);
                setWellWithOtherAnimals(JSON.parse(localStorage.getItem('form2')).wellWithOtherAnimals);
                setWhyChosenAdopt(JSON.parse(localStorage.getItem('form2')).whyChosenAdopt);
                setKnowNeedsAnimal(JSON.parse(localStorage.getItem('form2')).knowNeedsAnimal);
                setKnowHisExpenses(JSON.parse(localStorage.getItem('form2')).knowHisExpenses);
                setKnowHisDiet(JSON.parse(localStorage.getItem('form2')).knowHisDiet);
            }


            if (localStorage.getItem('form3')) {
                setPageForm(3);
                setTypeHouse(JSON.parse(localStorage.getItem('form3')).typeHouse);
                setLiveRenting(JSON.parse(localStorage.getItem('form3')).liveRenting);
                setHomeAllowsAnimals(JSON.parse(localStorage.getItem('form3')).homeAllowsAnimals);
                setMoveSoon(JSON.parse(localStorage.getItem('form3')).moveSoon);
                setHasGarden(JSON.parse(localStorage.getItem('form3')).hasGarden);
                setLiveOtherPeople(JSON.parse(localStorage.getItem('form3')).liveOtherPeople);
                setPeopleAgreeAdoption(JSON.parse(localStorage.getItem('form3')).peopleAgreeAdoption);
                setAgreeVisitHome(JSON.parse(localStorage.getItem('form3')).agreeVisitHome);
            }
        } else {
            setPageForm(1);
        }

    }, []);

    const valueForm1 = (e) => {

        const fullName = e.target.name === 'fullName' ? e.target.value : (localStorage.getItem('form1') ? JSON.parse(localStorage.getItem('form1')).fullName : '');
        setFullName(fullName);

        const email = e.target.name === 'email' ? e.target.value : (localStorage.getItem('form1') ? JSON.parse(localStorage.getItem('form1')).email : '');
        setEmail(email);

        const phone = e.target.name === 'phone' ? e.target.value : (localStorage.getItem('form1') ? JSON.parse(localStorage.getItem('form1')).phone : '');
        setPhone(phone);

        const dni = e.target.name === 'dni' ? e.target.value : (localStorage.getItem('form1') ? JSON.parse(localStorage.getItem('form1')).dni : '');
        setDni(dni);

        const address = e.target.name === 'address' ? e.target.value : (localStorage.getItem('form1') ? JSON.parse(localStorage.getItem('form1')).address : '');
        setAddress(address);

        const postalCode = e.target.name === 'postalCode' ? e.target.value : (localStorage.getItem('form1') ? JSON.parse(localStorage.getItem('form1')).postalCode : '');
        setPostalCode(postalCode);

        const city = e.target.name === 'city' ? e.target.value : (localStorage.getItem('form1') ? JSON.parse(localStorage.getItem('form1')).city : '');
        setCity(city);

        const form1 = { fullName, email, phone, dni, address, postalCode, city };

        localStorage.setItem('form1', JSON.stringify(form1));
    }

    const valueForm2 = (e) => {
        const haveOtherAnimals = e.target.name === 'haveOtherAnimals' ? e.target.value : (localStorage.getItem('form2') ? JSON.parse(localStorage.getItem('form2')).haveOtherAnimals : '');
        setHaveOtherAnimals(haveOtherAnimals);

        let whichOtherAnimals = e.target.name === 'whichOtherAnimals' ? e.target.value : (localStorage.getItem('form2') ? JSON.parse(localStorage.getItem('form2')).whichOtherAnimals : '');
        setWhichOtherAnimals(whichOtherAnimals);

        let wellWithOtherAnimals = e.target.name === 'wellWithOtherAnimals' ? e.target.value : (localStorage.getItem('form2') ? JSON.parse(localStorage.getItem('form2')).wellWithOtherAnimals : '');
        setWellWithOtherAnimals(wellWithOtherAnimals);

        const whyChosenAdopt = e.target.name === 'whyChosenAdopt' ? e.target.value : (localStorage.getItem('form2') ? JSON.parse(localStorage.getItem('form2')).whyChosenAdopt : '');
        setWhyChosenAdopt(whyChosenAdopt);

        const knowNeedsAnimal = e.target.name === 'knowNeedsAnimal' ? e.target.value : (localStorage.getItem('form2') ? JSON.parse(localStorage.getItem('form2')).knowNeedsAnimal : '');
        setKnowNeedsAnimal(knowNeedsAnimal);

        const knowHisExpenses = e.target.name === 'knowHisExpenses' ? e.target.value : (localStorage.getItem('form2') ? JSON.parse(localStorage.getItem('form2')).knowHisExpenses : '');
        setKnowHisExpenses(knowHisExpenses);

        const knowHisDiet = e.target.name === 'knowHisDiet' ? e.target.value : (localStorage.getItem('form2') ? JSON.parse(localStorage.getItem('form2')).knowHisDiet : '');
        setKnowHisDiet(knowHisDiet);

        if (haveOtherAnimals === "false") {
            setWhichOtherAnimals("");
            setWellWithOtherAnimals("");
            whichOtherAnimals = "";
            wellWithOtherAnimals = "";
        }
        const form2 = { haveOtherAnimals, whichOtherAnimals, wellWithOtherAnimals, whyChosenAdopt, knowNeedsAnimal, knowHisExpenses, knowHisDiet };

        localStorage.setItem('form2', JSON.stringify(form2));;
    }

    const valueForm3 = (e) => {

        const typeHouse = e.target.name === 'typeHouse' ? e.target.value : (localStorage.getItem('form3') ? JSON.parse(localStorage.getItem('form3')).typeHouse : '');
        setTypeHouse(typeHouse);

        const liveRenting = e.target.name === 'liveRenting' ? e.target.value : (localStorage.getItem('form3') ? JSON.parse(localStorage.getItem('form3')).liveRenting : '');
        setLiveRenting(liveRenting);

        const homeAllowsAnimals = e.target.name === 'homeAllowsAnimals' ? e.target.value : (localStorage.getItem('form3') ? JSON.parse(localStorage.getItem('form3')).homeAllowsAnimals : '');
        setHomeAllowsAnimals(homeAllowsAnimals);

        const moveSoon = e.target.name === 'moveSoon' ? e.target.value : (localStorage.getItem('form3') ? JSON.parse(localStorage.getItem('form3')).moveSoon : '');
        setMoveSoon(moveSoon);

        const hasGarden = e.target.name === 'hasGarden' ? e.target.value : (localStorage.getItem('form3') ? JSON.parse(localStorage.getItem('form3')).hasGarden : '');
        setHasGarden(hasGarden);

        const liveOtherPeople = e.target.name === 'liveOtherPeople' ? e.target.value : (localStorage.getItem('form3') ? JSON.parse(localStorage.getItem('form3')).liveOtherPeople : '');
        setLiveOtherPeople(liveOtherPeople);

        const peopleAgreeAdoption = e.target.name === 'peopleAgreeAdoption' ? e.target.value : (localStorage.getItem('form3') ? JSON.parse(localStorage.getItem('form3')).peopleAgreeAdoption : '');
        setPeopleAgreeAdoption(peopleAgreeAdoption);

        const agreeVisitHome = e.target.name === 'agreeVisitHome' ? e.target.value : (localStorage.getItem('form3') ? JSON.parse(localStorage.getItem('form3')).agreeVisitHome : '');
        setAgreeVisitHome(agreeVisitHome);

        const form3 = {
            typeHouse, liveRenting, homeAllowsAnimals, moveSoon, hasGarden,
            liveOtherPeople, peopleAgreeAdoption, agreeVisitHome
        }

        localStorage.setItem('form3', JSON.stringify(form3));
    }

    const onSubmit = formData => {

        console.log(formData);
        
        apiLucky.post('/user/adoptions', formData)
            .then(res => {
                const result = res;
                console.log(result);

                if (res.data.message !== "error") {
                    setSignUp(true);
                    setShowModal(true);
                    if (position) { setPosition(position); }

                    localStorage.removeItem('form1');
                    localStorage.removeItem('form2');
                    localStorage.removeItem('form3');
                    localStorage.removeItem('idPet');

                } else {
                    setSignUp(false);
                    setShowModal(true);
                    if (position) { setPosition(position); }
                }
            })

       
    }

    return (
        <div className="c-form-adoption d-flex flex-column alig-items-center">

            {pageForm === 1 && <>
                <div className="d-flex flex-row b-margin__top--big">
                    <Link to={"/adoption/pet/" + localStorage.getItem('idPet')} className="col-3 lucky-back-arrow b-color-salmon b-color-salmon--medium"></Link>
                    <h5>Formulario de adopción</h5>
                </div>
            </>}

            {pageForm !== 1 && <>
                <div className="d-flex flex-row b-margin__top--big">
                    <button className="col-3 lucky-back-arrow b-color-salmon b-color-salmon--medium" onClick={() => setPageForm(pageForm - 1)}></button>
                    <h5>Formulario de adopción</h5>
                </div>
            </>
            }

            <form className="pl-5 pr-5" onSubmit={handleSubmit(onSubmit)}>

                {/* Página 1 del formulario */}
                <div className={pageForm !== 1 ? "d-none" : ""}>
                    <ProgressBar value={33.33} className="mt-5 b-primereact-progressbar" showValue={false} />

                    <div className="d-flex flex-column align-items-start">
                        <h4 className="b-margin__top--medium b-margin__bottom--small">Tus datos</h4>
                        <input type="text" name="fullName" className="b-input b-input--big" placeholder={fullName === '' ? "Nombre y apellidos" : ""} value={fullName} onChange={($e) => valueForm1($e)} ref={register({ required: true })} />
                        <input type="email" name="email" className="b-input b-input--big" placeholder={email === '' ? "Email" : ""} value={email} onChange={($e) => valueForm1($e)} ref={register({ required: true })} />
                        <input type="text" name="phone" className="b-input b-input--big" placeholder={phone === '' ? "Teléfono" : ""} value={phone} onChange={($e) => valueForm1($e)} ref={register({ required: true })} />
                        <input type="text" name="dni" className="b-input b-input--big" placeholder={dni === '' ? "DNI" : ""} value={dni} onChange={($e) => valueForm1($e)} ref={register({ required: true })} />

                        <h4 className="b-margin__top--medium b-margin__bottom--small">Dirección</h4>
                        <input type="text" name="address" className="b-input b-input--big" placeholder={address === '' ? "Calle, número, piso" : ""} value={address} onChange={($e) => valueForm1($e)} ref={register({ required: true })} />
                        <input type="text" name="postalCode" className="b-input b-input--big" placeholder={postalCode === '' ? "Código postal" : ""} value={postalCode} onChange={($e) => valueForm1($e)} ref={register({ required: true })} />
                        <input type="text" name="city" className="b-input b-input--big" placeholder={city === '' ? "Ciudad" : ""} value={city} onChange={($e) => valueForm1($e)} ref={register({ required: true })} />


                        <div className="mb-5">
                            <input type="checkbox" name="acceptTerms" id="acceptTerms" checked={acceptTerms} onChange={($e) => setAcceptTerms($e.target.checked)} /> Acepto los términos y condiciones de la adopción
                        </div>

                        
                        {acceptTerms && <p className="col-9 mt-4 b-btn-rectangle b-btn-rectangle--big b-btn-rectangle--salmon-dark mb-5" onClick={() => setPageForm(2)}>Continuar</p>}
                    </div>

                </div>

                {/* Página 2 del formulario */}
                <div className={pageForm !== 2 ? "d-none" : ""}>
                    <ProgressBar value={66.66} className="mt-5 b-primereact-progressbar" showValue={false} />

                    <div className="d-flex flex-column align-items-start">
                        <h4 className="b-margin__top--medium">Sobre tus mascotas</h4>

                        <div className="b-margin__top--small b-margin__bottom--small d-flex flex-row justify-content-between">
                            <label className="b-label__form" htmlFor="haveOtherAnimals">¿Tienes otros animales?</label>
                            <div className="d-flex flex-row justify-content-end">
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" checked={haveOtherAnimals === 'true'} name="haveOtherAnimals" value={true} onChange={($e) => valueForm2($e)} ref={register()} />
                                    <span className="b-color-pineGreen">Sí</span>
                                </div>
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" checked={haveOtherAnimals === 'false'} name="haveOtherAnimals" value={false} onChange={($e) => valueForm2($e)} ref={register()} />
                                    <span className="b-color-pineGreen">No</span>
                                </div>
                            </div>
                        </div>

                        {haveOtherAnimals === 'true' && <>
                            <input type="text" name="whichOtherAnimals" className="b-input b-input--big" placeholder={whichOtherAnimals === '' ? "¿Cuáles?" : ""} value={whichOtherAnimals} onChange={($e) => valueForm2($e)} ref={register({ required: true })} />

                            <input type="text" name="wellWithOtherAnimals" className="b-input b-input--big" placeholder={wellWithOtherAnimals === '' ? "¿Se lleva bien con otros animales?" : ""} value={wellWithOtherAnimals} onChange={($e) => valueForm2($e)} ref={register({ required: true })} />
                        </>
                        }


                        <label className="b-label__form" htmlFor="whyChosenAdopt">¿Por qué has elegido adoptar?</label>
                        <input type="text" name="whyChosenAdopt" className="b-input b-input--big mt-4" value={whyChosenAdopt} onChange={($e) => valueForm2($e)} ref={register({ required: true })} />

                        <label className="b-label__form" htmlFor="knowNeedsAnimal">¿Conoces las necesidades del animal?</label>
                        <input type="text" name="knowNeedsAnimal" className="b-input b-input--big mt-4" value={knowNeedsAnimal} onChange={($e) => valueForm2($e)} ref={register({ required: true })} />

                        <label className="b-label__form" htmlFor="knowHisExpenses">¿Conoces sus gastos?</label>
                        <input type="text" name="knowHisExpenses" className="b-input b-input--big mt-4" value={knowHisExpenses} onChange={($e) => valueForm2($e)} ref={register({ required: true })} />

                        <label className="b-label__form" htmlFor="knowHisDiet">¿Conoces su alimentación?</label>
                        <input type="text" name="knowHisDiet" className="b-input b-input--big mt-4" value={knowHisDiet} onChange={($e) => valueForm2($e)} ref={register({ required: true })} />

                        <p className="col-9 mt-4 b-btn-rectangle b-btn-rectangle--big b-btn-rectangle--salmon-dark mb-5" onClick={() => setPageForm(3)}>Continuar</p>
                    </div>
                </div>
                

                {/* Página 3 del formulario */}
                <div className={pageForm !== 3 ? "d-none" : ""}>
                    <ProgressBar value={100} className="mt-5 b-primereact-progressbar" showValue={false} />
                    <div className="d-flex flex-column align-items-start">
                        <h4 className="b-margin__top--medium b-margin__bottom--small">Familia y hogar</h4>

                        <label htmlFor="typeHouse">¿Dónde vives?</label>
                        <input type="text" name="typeHouse" className="b-input b-input--big mt-4 b-margin__bottom--small" placeholder={typeHouse === '' ? "Piso, cada, chalet..." : ""} value={typeHouse} onChange={($e) => valueForm3($e)} ref={register({ required: true })} />

                        <div className="col-12 p-0 b-margin__top--small b-margin__bottom--small d-flex flex-row justify-content-between">
                            <p htmlFor="liveRenting">¿Vives de alquiler?</p>
                            <div className="d-flex flex-row justify-content-end">
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="liveRenting" checked={liveRenting === 'true'} value={true} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">Sí</span>
                                </div>
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="liveRenting" checked={liveRenting === 'false'} value={false} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">No</span>
                                </div>
                            </div>
                        </div>

                        <div className="col-12 p-0 b-margin__top--small b-margin__bottom--small d-flex flex-row justify-content-between">
                            <p htmlFor="homeAllowsAnimals">¿Tú casero permite animales?</p>
                            <div className="d-flex flex-row justify-content-end">
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="homeAllowsAnimals" checked={homeAllowsAnimals === 'true'} value={true} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">Sí</span>
                                </div>
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="homeAllowsAnimals" checked={homeAllowsAnimals === 'false'} value={false} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">No</span>
                                </div>
                            </div>
                        </div>

                        <div className="col-12 p-0 b-margin__top--small b-margin__bottom--small d-flex flex-row justify-content-between">
                            <p className="p--medium" htmlFor="moveSoon">¿Crees que podrías mudarte pronto?</p>
                            <div className="d-flex flex-row justify-content-end">
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="moveSoon" checked={moveSoon === 'true'} value={true} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">Sí</span>
                                </div>
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="moveSoon" checked={moveSoon === 'false'} value={false} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">No</span>
                                </div>
                            </div>
                        </div>

                        <div className="col-12 p-0 b-margin__top--small b-margin__bottom--small d-flex flex-row justify-content-between">
                            <p htmlFor="hasGarden">¿Tiene jardín?</p>
                            <div className="d-flex flex-row justify-content-end">
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="hasGarden" checked={hasGarden === 'true'} value={true} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">Sí</span>
                                </div>
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="hasGarden" checked={hasGarden === 'false'} value={false} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">No</span>
                                </div>
                            </div>
                        </div>

                        <div className="col-12 p-0 b-margin__top--small b-margin__bottom--small d-flex flex-row justify-content-between">
                            <p htmlFor="liveOtherPeople">¿Vives con otras personas?</p>
                            <div className="d-flex flex-row justify-content-end">
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="liveOtherPeople" checked={liveOtherPeople === 'true'} value={true} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">Sí</span>
                                </div>
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="liveOtherPeople" checked={liveOtherPeople === 'false'} value={false} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">No</span>
                                </div>
                            </div>
                        </div>

                        <div className="col-12 p-0 b-margin__top--small b-margin__bottom--small d-flex flex-row justify-content-between">
                            <p className="p--medium" htmlFor="peopleAgreeAdoption">¿Están todos de acuerdo con la adopción?</p>
                            <div className="d-flex flex-row justify-content-end">
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="peopleAgreeAdoption" checked={peopleAgreeAdoption === 'true'} value={true} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">Sí</span>
                                </div>
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="peopleAgreeAdoption" checked={peopleAgreeAdoption === 'false'} value={false} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">No</span>
                                </div>
                            </div>
                        </div>

                        <div className="col-12 p-0 b-margin__top--small b-margin__bottom--small d-flex flex-row justify-content-between">
                            <p className="p--medium" htmlFor="agreeVisitHome">¿Estás de acuerdo con que visitemos tu casa?</p>
                            <div className="d-flex flex-row justify-content-end">
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="agreeVisitHome" checked={agreeVisitHome === 'true'} value={true} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">Sí</span>
                                </div>
                                <div>
                                    <input type="radio" className="ml-5 b-color-pineGreen" name="agreeVisitHome" checked={agreeVisitHome === 'false'} value={false} onChange={($e) => valueForm3($e)} ref={register()} />
                                    <span className="b-color-pineGreen">No</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <input type="text" name="adoptionPetUser" id="adoptionPetUser" className="d-none" value={localStorage.getItem('idPet')} ref={register({ required: true })}/>
                    
                    <input type="submit" className="col-11 mt-4 mb-5 b-btn-rectangle b-btn-rectangle--big b-btn-rectangle--salmon-dark" value="Enviar" />
                </div>
            </form>

            {/* Modal */}
            <div className="b-primereact-dialog">
                <Dialog visible={showModal} onHide={() => onHide()}>
                    <ModalSend signUp={signUp} saved={'adoption'} />
                </Dialog>
            </div>
        </div>
    )
}
