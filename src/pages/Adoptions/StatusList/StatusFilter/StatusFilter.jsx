import React, { useState, useEffect, useContext } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import BtnIcon from '../../../../shared/components/BtnIcon/BtnIcon';
import statePetServices from '../../../../shared/services/statePetServices.json';

export default function StatusFilter() {
    const [stateSelected, setStateSelected] = useState(false);
    const [btnSelected, setBtnSelected] = useState(false);

    useEffect(() => {
        if (localStorage.getItem('StatePetFilter')) {
            const resultState = JSON.parse(localStorage.getItem('StatePetFilter')).statePet;

            resultState.map(specie => {
                statePetServices.map(item => {
                    if (item.icon === specie) {
                        item.selected = true;
                    }
                })
            })
        }

        if (statePetServices.findIndex(i => i.selected) >= 0) {
            setBtnSelected(true);
        } else {
            setBtnSelected(false);
            localStorage.removeItem('StatePetFilter');
        }
    }, [])

    const limpiarFilters = () => {
        statePetServices.map(item => {
            item.selected = false;
        })

        localStorage.removeItem('StatePetFilter');
        setBtnSelected(false);
    }

    const bntStatePet = (item) => {
        setStateSelected(!stateSelected);

        statePetServices[item.id].selected = !statePetServices[item.id].selected;

        let arrStatePet = JSON.parse(localStorage.getItem('StatePetFilter')) ? JSON.parse(localStorage.getItem('StatePetFilter')).statePet : [];

        if (statePetServices[item.id].selected) {
            arrStatePet.push(item.name);
        } else {
            arrStatePet = arrStatePet.filter(value => value !== statePetServices[item.id].name);
        }

        localStorage.setItem('StatePetFilter', JSON.stringify({ statePet: arrStatePet }));

        if (statePetServices.findIndex(i => i.selected) >= 0) {
            setBtnSelected(true);
        } else {
            setBtnSelected(false);
            localStorage.removeItem('StatePetFilter');
        }
    }

    return (
        <div className="c-status-filter">
            <div className="c-filter__buttons" id="animalBtns">
                {
                    statePetServices.map((item, index) => {
                        return <BtnIcon key={index} onClick={() => bntStatePet(item)} selected={stateSelected} item={item} />
                    })
                }
            </div>

            {/* Muestra el botón de aplicar si no hay elementos (btonIcons) seleccionados */}
            {!btnSelected && <div className="row justify-content-center m-0 mb-5">
                <Link to="/filterpet/result" className="col-11 b-btn-rectangle b-btn-rectangle--medium b-btn-rectangle--salmon-dark" >Aplicar</Link>
            </div>
            }

            {/* Muestra el botón de aplicar y borrar cuando hay por lo menos un elemento (btonIcons) seleccionado */}
            {btnSelected && <div className="row justify-content-center m-0 mb-5">
                <button className="col-5 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light" onClick={() => limpiarFilters()}>Borrar filtros</button>
                <Link to="/filterpet/result" className="col-5 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark">Aplicar</Link>
            </div>
            }
        </div>
    )
}
