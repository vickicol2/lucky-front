import React, { useState, useEffect, useContext } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";
import { Dialog } from 'primereact/dialog';
import BtnIcon from '../../../shared/components/BtnIcon/BtnIcon';
import PortaitAdoption from '../../../shared/components/PortaitAdoption/PortaitAdoption';
import statePetServices from '../../../shared/services/statePetServices.json';
import { actualPageContext } from '../../../shared/contexts/actualPageContext';
import { apiLucky } from '../../../shared/services/apiLucky';

export default function Status() {
    const [statePet, setStatePet] = useState([]);
    const [petsSearch, setPetsSearch] = useState([]);
    const [petsResultFilter, setPetsResultFilter] = useState([]);
    const [petsAdoptions, setPetsAdoptions] = useState()
    const [page, setPage] = useContext(actualPageContext);
    const [backPage, setBackPage] = useState(page);
    const [hasAdoptions, setHasAdoptions] = useState(false);

    const [stateSelected, setStateSelected] = useState(false);
    const [btnSelected, setBtnSelected] = useState(false);

    // Modal
    const [showModal, setShowModal] = useState(false);
    const [position, setPosition] = useState('center');


    const onHide = () => {
        setShowModal(false);
        localStorage.getItem('StatePetFilter') ? setStatePet(JSON.parse(localStorage.getItem('StatePetFilter')).statePet) : setStatePet([]);
    };

    useEffect(() => {
        apiLucky.get('/user/adoptions/' + localStorage.getItem('id'))
            .then(res => {
                if (res.data.message !== "error") {
                    const arrAdoptions = res.data.adoptions;
                    return arrAdoptions;
                } else {
                    return false;
                }
            })
            .then(res => {
                const pets = [];
                if (res) {
                    res.map(item => {
                        pets.push(item.adoptionPetUser);
                    })
                    setPetsAdoptions(pets);
                    setPetsSearch(pets);
                    setHasAdoptions(true);
                    console.log(pets)
                } else {
                    setHasAdoptions(false);
                }
            })

        if (localStorage.getItem('StatePetFilter')) {
            const resultState = JSON.parse(localStorage.getItem('StatePetFilter')).statePet;

            resultState.map(specie => {
                statePetServices.map(item => {
                    if (item.icon === specie) {
                        item.selected = true;
                    }
                })
            })
        }

        if (statePetServices.findIndex(i => i.selected) >= 0) {
            setBtnSelected(true);
        } else {
            setBtnSelected(false);
            localStorage.removeItem('StatePetFilter');
        }

    }, [])

    useEffect(() => {

        setPage('other');

        let result = [];

        if (statePet.length > 0) {
            statePet.map(statePet => {
                petsAdoptions.map(item => {
                    if (item.statePet === statePet) {
                        result.push(item)
                    }
                })
            })
            setPetsSearch(result);
            setPetsResultFilter(result);
        } else {
            setPetsSearch(petsAdoptions);
            setPetsResultFilter(petsAdoptions);
        }


    }, [backPage, statePet])

    const searchPet = (value) => {

        if (localStorage.getItem('StatePetFilter')) {

            const result = petsResultFilter.filter(item => item.name.toLowerCase().includes(value.toLowerCase()) ||
                item.specie.toLowerCase().includes(value.toLowerCase()) ||
                item.gender.toLowerCase().includes(value.toLowerCase()) ||
                item.statePet.toLowerCase().includes(value.toLowerCase()) ||
                item.city.toLowerCase().includes(value.toLowerCase()) ||
                item.size.toLowerCase().includes(value.toLowerCase()));
            setPetsSearch(result);
        } else {
            const result = petsAdoptions.filter(item => item.name.toLowerCase().includes(value.toLowerCase()) ||
                item.specie.toLowerCase().includes(value.toLowerCase()) ||
                item.gender.toLowerCase().includes(value.toLowerCase()) ||
                item.statePet.toLowerCase().includes(value.toLowerCase()) ||
                item.city.toLowerCase().includes(value.toLowerCase()) ||
                item.size.toLowerCase().includes(value.toLowerCase()));
            setPetsSearch(result);
        }
    }

    const limpiarFilters = () => {
        statePetServices.map(item => {
            item.selected = false;
        })

        localStorage.removeItem('StatePetFilter');
        setBtnSelected(false);
    }

    const bntStatePet = (item) => {
        setStateSelected(!stateSelected);

        statePetServices[item.id].selected = !statePetServices[item.id].selected;

        let arrStatePet = JSON.parse(localStorage.getItem('StatePetFilter')) ? JSON.parse(localStorage.getItem('StatePetFilter')).statePet : [];

        if (statePetServices[item.id].selected) {
            arrStatePet.push(item.name);
        } else {
            arrStatePet = arrStatePet.filter(value => value !== statePetServices[item.id].name);
        }

        localStorage.setItem('StatePetFilter', JSON.stringify({ statePet: arrStatePet }));

        if (statePetServices.findIndex(i => i.selected) >= 0) {
            setBtnSelected(true);
        } else {
            setBtnSelected(false);
            localStorage.removeItem('StatePetFilter');
        }
    }

    const openStateFilter = () => {
        setShowModal(true);
        if (position) {
            setPosition(position);
        }
    }


    return (
        <>
            {petsSearch && hasAdoptions &&

                <div className="c-animals">
                    <div className="row m-0 mt-5 mb-5 align-items-center justify-content-center">
                        <Link to={backPage === 'profile' ? '/profile' : '/adoption'} className="col-1 col-md-2 p-0 b-color-salmon b-color-salmon--big"><span className="lucky-back-arrow"></span></Link>

                        {/* Buscar... */}
                        <div className="col-8 col-md-7">
                            <input type="search" className="c-input-search__text" placeholder="Buscar" onChange={$event => { searchPet($event.target.value) }} />
                            <span className="lucky-search c-input-search__icon"></span>
                        </div>

                        <button className="col-1 col-md-2 p-0 b-color-salmon b-color-salmon--big" onClick={() => { openStateFilter() }}><span className="lucky-filter"></span></button>
                    </div>

                    {petsSearch.length > 0 ? <PortaitAdoption petsAdoptions={petsSearch} type={'list'} /> : <h4 className="mt-5">No hay resultados para su búsqueda...</h4>}
                </div>
            }

            {!hasAdoptions && <div className="d-flex flex-column align-items-center b-margin__top--extrabig">
                <div className="d-flex flex-row align-items-center mb-5">
                    <Link to={backPage === 'profile' ? '/profile' : '/adoption'} className="col-1 col-md-2 p-0 b-color-salmon b-color-salmon--big"><span className="lucky-back-arrow"></span></Link>
                    <h4>¡Aún no tienes solicitudes de adopción!</h4>
                </div>
                <img className="b-img b-img--big" src="/img/onboarding3.png" alt="Logo Lucky" />
            </div>
            }


            {/* Modal */}
            <div className="b-primereact-dialog">
                <Dialog visible={showModal} onHide={() => onHide()}>
                    <div className="b-box-filter">
                        <div className="c-filter__buttons d-flex flex-row flex-wrap" id="animalBtns">
                            {
                                statePetServices.map((item, index) => {
                                    return <BtnIcon key={index} onClick={() => bntStatePet(item)} selected={stateSelected} item={item} />
                                })
                            }
                        </div>

                        {/* Muestra el botón de aplicar si no hay elementos (btonIcons) seleccionados */}
                        {!btnSelected && <div className="row justify-content-center m-0 mb-5">
                            <button className="col-11 b-btn-rectangle b-btn-rectangle--medium b-btn-rectangle--salmon-dark" onClick={() => onHide()} >Aplicar</button>
                        </div>
                        }

                        {/* Muestra el botón de aplicar y borrar cuando hay por lo menos un elemento (btonIcons) seleccionado */}
                        {btnSelected && <div className="row justify-content-center m-0 mb-5">
                            <button className="col-5 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light" onClick={() => limpiarFilters()}>Borrar filtros</button>
                            <button className="col-5 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark" onClick={() => onHide()}>Aplicar</button>
                        </div>
                        }
                    </div>
                </Dialog>
            </div>
        </>
    )
}
