import React, { useState, useEffect, useContext } from 'react';
import { useParams, Link } from "react-router-dom";
import { TabView, TabPanel } from 'primereact/tabview';
import { Calendar } from 'primereact/calendar';
import { useForm } from 'react-hook-form';
import { apiLucky } from '../../../../shared/services/apiLucky';
import { FileUpload } from 'primereact/fileupload';
import PortaitAdoption from '../../../../shared/components/PortaitAdoption/PortaitAdoption';
import BtonArrow from '../../../../shared/components/BtnArrow/BtnArrow';
import { actualPageContext } from '../../../../shared/contexts/actualPageContext';

export default function StatusDetails() {
    const id = useParams().id;
    const [activeIndex, setActiveIndex] = useState(0);  // Para el tabview
    const { register, handleSubmit, watch, errors } = useForm();
    const [payApp, setPayApp] = useState(false);
    const [talkToShelter, setTalkToShelter] = useState(false);
    const [time, setTime] = useState(null);
    const [page, setPage] = useContext(actualPageContext);
    const [petDetails, setPetDetails] = useState();
    const [statePet, setStatePet] = useState(false);

    useEffect(() => {
        setPage('other');

        apiLucky.get('/user/adoption/' + id)  // id de la mascota
            .then(res => {
                console.log(res.data.pet);

                if (res.data.message !== "error") {
                    setPetDetails(res.data.pet);
                    // setStatePet(true);
                } 
                // else {
                //     setPetDetails(false);
                //     // setStatePet(true);
                // }

            })


    }, [])

    const onSubmit = formData => {
        console.log(formData);


    }

    return (
        <> {petDetails &&

            <div className="c-status-details">
                <div className="d-flex flex-row b-margin__top--big">
                    <Link to="/state/list" className="col-3 lucky-back-arrow b-color-salmon b-color-salmon--big"></Link>
                    <h5 className="b-color-pineGreen b-color-pineGreen--big">Adopción de {petDetails.name}</h5>
                </div>

                <TabView className="b-primereact-tabview" activeIndex={activeIndex} onTabChange={(e) => setActiveIndex(e.index)}>
                    <TabPanel header="Resumen">

                        <PortaitAdoption petsAdoptions={petDetails} type={'details'} />

                        <div className="b-separator"></div>

                        <div className="d-flex flex-column align-items-center b-margin__top--medium b-margin__bottom--medium">
                            {/* Titulos */}
                            <div className="d-flex flex-row">
                                <div className="c-login__img mr-5">
                                    <img src="/img/amigopet.png" alt="Amigo Pet" />
                                </div>

                                <div className="d-flex flex-column">
                                    <h4 className="mb-4">Asociación Protectora Amigo Pet</h4>
                                    <p><span className="lucky-localization mr-3 b-color-ocean b-color-ocean--big"></span>Av Reina Lucia, 57, 12321, Madrid</p>
                                    <div className="d-flex flex-row align-items-center mt-3 ml-4">
                                        <p>Contacta con nosotros</p>
                                        <p className="lucky-email ml-5 mr-5 b-color-salmon b-color-salmon--big"></p>
                                        <p className="lucky-whatsapp b-color-salmon b-color-salmon--big"></p>
                                    </div>
                                </div>
                            </div>

                            {/* Google Maps */}
                            {/* <div className="mt-5 mb-5">
                                Google Maps
                            </div> */}

                            {/* Contactos */}
                        </div>
                    </TabPanel>

                    <TabPanel header="Info Adicional">
                        <div className="d-flex flex-column align-items-start">
                            <label className="mb-4">Subir imágenes</label>
                            <p>Necesitamos que nos subas algunas fotos de dónde va vivir tu nueva mascota para poder echarte una mano si necesitas algo más de información</p>

                            <form className="c-login__form mt-5" onSubmit={handleSubmit(onSubmit)}>
                                <div className="d-flex flex-row mb-5">
                                    <div className="b-primereact-upload mr-5">
                                        <FileUpload name="img" mode="basic" />
                                    </div>

                                    <div className="b-primereact-upload mr-5">
                                        <FileUpload name="img" mode="basic" />
                                    </div>

                                    <div className="b-primereact-upload mr-5">
                                        <FileUpload name="img" mode="basic" />
                                    </div>
                                </div>

                                <div className="b-separator mt-5 mb-5"></div>

                                <div className="d-flex flex-column align-items-start">

                                    <label className="mb-4">¿Cómo quires pagar las tasas?</label>
                                    <p className="mb-5">Para pagar las tasas de adopción puedes elegir o pagarlo mediante la app con un pago único o poniéndose en contacto con la protectora para fraccionar el pago</p>


                                    <select className="mb-5 c-btn-arrow m-0" name="rate" id="">
                                        <option value="125">125 €</option>
                                        <option value="100">100 €</option>
                                        <option value="50">50 €</option>
                                    </select>

                                    <div className="mb-5 d-flex flex-row align-items-center">
                                        <input type="checkbox" name="payApp" id="payApp" checked={payApp} onChange={($e) => setPayApp($e.target.checked)} />
                                        <p>Pagar a través de la aplicación</p>
                                        <span className="lucky-info b-color-salmon b-color-salmon--medium ml-4"></span>
                                    </div>

                                    <div className="mb-5 d-flex flex-row align-items-center">
                                        <input type="checkbox" name="talkToShelter" id="talkToShelter" checked={talkToShelter} onChange={($e) => setTalkToShelter($e.target.checked)} />
                                        <p>Hablar con la protectora</p>
                                        <span className="lucky-info b-color-salmon b-color-salmon--medium ml-4"></span>
                                    </div>
                                </div>

                                <input type="submit" className="col-11 mt-4 b-btn-rectangle b-btn-rectangle--big b-btn-rectangle--salmon-dark" value="Enviar" />
                            </form>
                        </div>
                    </TabPanel>


                    <TabPanel header="Adopción">
                        <label className="mb-4">Dirección</label>
                        <div className="d-flex flex-column align-items-center b-margin__top--medium">
                            <div className="d-flex flex-row">
                                <div className="c-login__img mr-5">
                                    <img src="/img/amigopet.png" alt="Amigo Pet" />
                                </div>

                                <div className="d-flex flex-column">
                                    <h4 className="mb-4">Asociación Protectora Amigo Pet</h4>
                                    <p><span className="lucky-localization mr-3 b-color-ocean b-color-ocean--big"></span>Av Reina Lucia, 57, 12321, Madrid</p>
                                    <div className="d-flex flex-row align-items-center mt-3 ml-4">
                                        <p>Contacta con nosotros</p>
                                        <p className="lucky-email ml-5 mr-5 b-color-salmon b-color-salmon--big"></p>
                                        <p className="lucky-whatsapp b-color-salmon b-color-salmon--big"></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="b-separator mt-5 mb-5"></div>

                        <form className="c-login__form mt-5 d-flex flex-column align-items-start" onSubmit={handleSubmit(onSubmit)}>

                            <label className="mb-4 al">Día</label>
                            <input type="date" name="day" id="day" className="b-input b-input--big" />

                            <label className="b-select__label mb-4" htmlFor="city">¿A qué hora puedes venir?</label>
                            <Calendar placeholder="00:00" timeOnly hourFormat="24" className="b-primereact-calendar b-margin__bottom--big w-100" value={time} onChange={(e) => setTime(e.value)}></Calendar>

                            <input type="submit" className="col-11 mt-5 b-btn-rectangle b-btn-rectangle--big b-btn-rectangle--salmon-dark" value="Enviar" />
                        </form>
                    </TabPanel>
                </TabView>

            </div>
        }</>

    )
}
