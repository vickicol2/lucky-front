import React, { useState, useEffect, useContext } from 'react';
import './Login.scss';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { apiLucky } from '../../../shared/services/apiLucky';
import { isLoggedContext } from '../../../shared/contexts/isLoggedContext';
import { actualPageContext } from '../../../shared/contexts/actualPageContext';

export default function Login(props) {
    const [isLogged, setIsLogged] = useContext(isLoggedContext);
    const [page, setPage] = useContext(actualPageContext);
    const [hidePassword, setHidePassword] = useState(true);
    const { register, handleSubmit, watch, errors } = useForm();
    const [response, setResponse] = useState(true);
    let history = useHistory();


    useEffect(() => {
        setPage('other');
    }, [])

    const onSubmit = formData => {

        if (!!localStorage.getItem('userType')) {
            apiLucky.post('/' + localStorage.getItem('userType') + '/login', formData)
                .then(res => {
                    // console.log(res.data.message);

                    if (res.data.message !== "error"){
                        localStorage.setItem('token', res.data.token);
                        localStorage.getItem('userType') === 'user' ? localStorage.setItem('id', res.data.user._id) : localStorage.setItem('id', res.data.shelter._id)
                        setIsLogged(true);
                        setResponse(true);
                        history.push('/home');
                    } else {
                        setIsLogged(false);
                        setResponse(false);
                    }
                })
        }
    }


    return (
        <div className="c-login">
            <div className="c-login__header">
                <div className="c-login__img">
                    <img src="/img/logo.png" alt="Logo Lucky" />
                </div>
                <h1 className="c-login__primary-title">LUCKY</h1>
            </div>

            <h2 className="c-login__second-title">¡Hola! para continuar, inicia sesión o crea una cuenta</h2>

            <form className="c-login__form" onSubmit={handleSubmit(onSubmit)}>
                <input type="email" name="email" id="email" className="b-input" placeholder="ejemplo@lucky.com" ref={register({ required: true, pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ })} />
                {errors.name && errors.name.type === 'required' && <span>Este campo es requerido</span>}

                <div className="b-input-password">
                    <input type={hidePassword ? 'password' : 'text'} name="password" id="password" className="b-input-password__password" ref={register({ required: true, minLength: 8 })} />
                    <p className="lucky-eye b-input-password__icon" onClick={() => setHidePassword(!hidePassword)}></p>
                </div>
                <p className="b-input-password__p">¿Has olvidado tu contraseña?</p>
                {errors.name && errors.name.type === 'required' && <span>Este campo es requerido</span>}
                {errors.name && errors.name.type === 'minLength' && <span>La longitud mínima es de 8 carácteres</span>}

                {!response  && <label className="mt-4 b-color-salmon">Usuario y/o contraseña incorrectas</label> }
                <div className="row justify-content-center m-0 mt-5">
                    <input type="submit" className="col-11 mt-4 b-btn-rectangle b-btn-rectangle--big b-btn-rectangle--ocean-dark" value="Iniciar Sesión" />
                    <Link to="/register/user" className="col-11 mt-4 b-btn-rectangle b-btn-rectangle--big b-btn-rectangle--ocean-light">Crear cuenta</Link>
                </div>
            </form>


        </div>
    )
}
