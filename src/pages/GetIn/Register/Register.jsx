import React, { useState, useEffect, useContext } from 'react';
import './Register.scss';
import { Link } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { useForm } from 'react-hook-form';
import { apiLucky } from '../../../shared/services/apiLucky';
import ModalSend from '../../../shared/components/ModalSend/ModalSend';
import { actualPageContext } from '../../../shared/contexts/actualPageContext';

const city = ['Madrid', 'Barcelona', 'Granada', 'Valencia', 'Sevilla'];

export default function Register() {
    const { register, handleSubmit, errors } = useForm();
    const [signUp, setSignUp] = useState(false);
    const [page, setPage] = useContext(actualPageContext);
    const [hidePassword, setHidePassword] = useState(true);
    const [response, setResponse] = useState(true);

    // Modal
    const [showModal, setShowModal] = useState(false);
    const [position, setPosition] = useState('center');

    const onHide = () => {
        setShowModal(false);
    };

    useEffect(() => {
        setPage('other');
    }, [])


    const onSubmit = formData => {
        formData.img = formData.img[0];

        const getFormData = object => Object.keys(object).reduce((formData, key) => {
            formData.append(key, object[key]);
            return formData;
        }, new FormData());

        if (!!localStorage.getItem('userType')) {
            apiLucky.post('/' + localStorage.getItem('userType') + '/signup', getFormData(formData))
                .then(res => {
                    console.log(res);

                    if (res.data.message !== "email ya existe") {
                        setSignUp(true);
                        setShowModal(true);
                        if (position) { setPosition(position); }



                    } else {
                        setSignUp(false);
                        setResponse(false);
                    }
                })
        }
    }

    return (
        <div className="c-register">
            <div className="c-register__header">
                <div className="c-register__img">
                    <img src="/img/logo.png" alt="Logo Lucky" />
                </div>
                <h1 className="c-register__primary-title">LUCKY</h1>
            </div>

            <h4 className="mt-5 mb-5">{localStorage.getItem('userType') === 'user' ? "Registro de Usuario" : "Registro de Asociación protectora"} </h4>

            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="c-register__form mt-5 pl-5">
                    <label htmlFor={localStorage.getItem('userType') === 'user' ? "fullname" : "name"} className="mb-4">Nombre</label>
                    {errors.name && errors.name.type === 'required' && <span className="mb-5 ">Este campo es requerido</span>}
                    <input type="text" className="b-input" name={localStorage.getItem('userType') === 'user' ? "fullname" : "name"} id={localStorage.getItem('userType') === 'user' ? "fullname" : "name"} ref={register({ required: true })} />

                    <label className="mb-3" htmlFor="address">Dirección</label>
                    {errors.name && errors.name.type === 'required' && <span>Este campo es requerido</span>}
                    <textarea className="b-input" name="address" id="address" ref={register({ required: true })}></textarea>


                    {localStorage.getItem('userType') === 'user' && <>
                        <label htmlFor="postalCode" className="mb-4">Código Postal</label>
                        {errors.name && errors.name.type === 'required' && <span>Este campo es requerido</span>}
                        <input type="text" className="mb-5 b-input" name="postalCode" id="postalCode" ref={register({ required: true })} />

                        {/* <Select por ciudad /> */}
                        <div className="b-select b-select--medium mt-5">
                            <label className="b-select__label b-select__label mb-3" htmlFor="city">Ciudad</label>
                            {errors.name && errors.name.type === 'required' && <span>Este campo es requerido</span>}
                            <select name="city" id="city" className="b-select__content" ref={register({ required: true })}>
                                {
                                    city.map((item, index) => {
                                        return <option key={index} value={item} className="b-select__opcion">{item}</option>
                                    })
                                }
                            </select>
                        </div>

                        <label htmlFor="dni" className="mb-4 mt-4">DNI</label>
                        {errors.name && errors.name.type === 'required' && <span>Este campo es requerido</span>}
                        <input type="text" className="mb-5 b-input" name="dni" id="dni" ref={register({ required: true })} />
                    </>
                    }

                    <label htmlFor="phone" className="mb-4 mt-5">Teléfono</label>
                    {errors.name && errors.name.type === 'required' && <span>Este campo es requerido</span>}
                    <input type="text" className="mb-5 b-input mt-5" name="phone" id="phone" ref={register({ required: true })} />

                    <label htmlFor="img" className="mb-4 mt-5">Foto de perfil</label>
                    {errors.name && errors.name.type === 'required' && <span>Este campo es requerido</span>}
                    <input type="file" className="mt-5 mb-5 b-input" name="img" id="img" ref={register({ required: true })} />


                    <label htmlFor="email" className="mb-4 mt-5">Email</label>
                    {errors.name && errors.name.type === 'required' && <span>Este campo es requerido</span>}
                    {!response && <label className="mt-4 b-color-salmon">El email ya existe</label>}
                    <input type="email" name="email" id="email" className="b-input mt-5" placeholder="ejemplo@lucky.com" ref={register({ required: true, pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ })} />

                    <label htmlFor="password" className="mb-4 mt-5">Contraseña</label>
                    {errors.name && errors.name.type === 'required' && <span>Este campo es requerido</span>}
                    {errors.name && errors.name.type === 'minLength' && <span>La longitud mínima es de 5 carácteres</span>}
                    <div className="b-input-password">
                        <input type={hidePassword ? 'password' : 'text'} name="password" id="password" className="b-input-password__password" ref={register({ required: true, minLength: 8 })} />
                        <p className="lucky-eye b-input-password__icon" onClick={() => setHidePassword(!hidePassword)}></p>
                    </div>

                </div>

                <div className="row justify-content-center m-0 mb-5 mt-5">
                    <Link to="/getin" className="col-5 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark">Cancelar</Link>
                    <input type="submit" className="col-5 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light" value="Registrarse" />
                </div>
            </form>

            {/* Modal de Enviado */}
            <div className="b-primereact-dialog">
                <Dialog visible={showModal} onHide={() => onHide()}>
                    <ModalSend signUp={signUp} saved={localStorage.getItem('userType')} redirect={'/getin'} />
                </Dialog>
            </div>
        </div>
    )
}
