import React, { useState, useEffect, useContext } from 'react';
import './GetIn.scss';
import { Link } from "react-router-dom";
import { actualPageContext } from '../../shared/contexts/actualPageContext';


export default function GetIn() {
    const [page, setPage] = useContext(actualPageContext);

    useEffect(() => {
        setPage('other')
    }, [])

    return (
        <div className="c-getin">

            <div className="c-getin__content m-auto">
                <h3>¿Cómo quieres entrar?</h3>

                <div className="row justify-content-center m-0">
                    <Link to="/login" className="col-9 mt-4 b-btn-rectangle b-btn-rectangle--big b-btn-rectangle--ocean-dark" onClick={() => localStorage.setItem('userType', 'user')}>Usuario</Link>
                    <Link to="/login" className="col-9 mt-4 b-btn-rectangle b-btn-rectangle--big b-btn-rectangle--ocean-dark" onClick={() => localStorage.setItem('userType', 'shelter')}>Asociación protectora</Link>
                    <Link to="/" className="col-11 mt-4 text-center c-getin__link">Registrarse en otro momento</Link>
                </div>


            </div>
        </div>
    )
}
