import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { useForm } from 'react-hook-form';
import { apiLucky } from '../../../../shared/services/apiLucky';
import ModalSend from '../../../../shared/components/ModalSend/ModalSend';
import { actualPageContext } from '../../../../shared/contexts/actualPageContext';

const city = ['Madrid', 'Barcelona', 'Granada', 'Valencia', 'Sevilla'];

export default function FormEdit() {
    const [page, setPage] = useContext(actualPageContext);
    const { register, handleSubmit, errors } = useForm();
    const [profile, setProfile] = useState();

    // Modal
    const [showModal, setShowModal] = useState(false);
    const [position, setPosition] = useState('center');
    const [signUp, setSignUp] = useState(false);

    useEffect(() => {
        setPage('other');

        // Quite el if principal del token
        if (localStorage.getItem('userType') === 'user') {
            apiLucky.get('/user/profile/')
                .then(res => {
                    if (res.data.message !== "error") { setProfile(res.data); }
                })
        } else if (localStorage.getItem('userType') === 'shelter') {
            apiLucky.get('/shelter/profile/' + localStorage.getItem('id'))
                .then(res => {
                    // console.log(res)
                    if (res.data.message !== "error") { setProfile(res.data.shelter); }
                })
        }
    }, []);

    const onHide = () => {
        setShowModal(false);
    };


    const onSubmit = formData => {
        console.log(formData)

        apiLucky.put('/' + localStorage.getItem('userType') + '/profile/', formData)
            .then(res => {
                console.log(res);

                if (res.data.message !== "error") {
                    setSignUp(true);
                    setShowModal(true);
                    if (position) { setPosition(position); }

                } else {
                    setSignUp(false);
                    setShowModal(true);
                    if (position) { setPosition(position); }
                }
            })
    }

    return (
        <>{profile && <div className="c-register">
            <div className="c-register__header">
                <div className="c-register__img">
                    <img src="/img/logo.png" alt="Logo Lucky" />
                </div>
                <h1 className="c-register__primary-title">LUCKY</h1>
            </div>

            <h4 className="mt-5 mb-5">Edita tus datos</h4>

            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="c-register__form mt-5 pl-5">
                    <label htmlFor={localStorage.getItem('userType') === 'user' ? "fullname" : "name"} className="mb-4">Nombre</label>
                    {errors.name && errors.name.type === 'required' && <span className="mb-5 ">Este campo es requerido</span>}
                    <input type="text" className="b-input" name={localStorage.getItem('userType') === 'user' ? "fullname" : "name"} id={localStorage.getItem('userType') === 'user' ? "fullname" : "name"} ref={register({ required: true })} />

                    <label>Dirección</label>
                    <input type="text" className="b-input mt-4" name="address" id="address" ref={register({ required: true })}></input>


                    {localStorage.getItem('userType') === 'user' && <>
                        <label htmlFor="postalCode" className="mb-4">Código Postal</label>
                        <label className="b-input mt-4">{profile.postalCode}</label>
                        <input type="text" className="d-none" name="postalCode" id="postalCode" value={profile.postalCode} ref={register({ required: true })} />

                        <label className="b-select__label b-select__label mb-3" htmlFor="city">Ciudad</label>
                        <label className="b-input mt-4">{profile.city}</label>
                        <input type="text" className="d-none" name="city" id="city" value={profile.city} ref={register({ required: true })} />

                        <label htmlFor="phone" className="mb-4">Teléfono</label>
                        <label className="b-input mt-4">{profile.phone}</label>
                        <input type="text" className="d-none" name="phone" id="phone" value={profile.phone} ref={register({ required: true })} />
                    </>
                    }

                    {localStorage.getItem('userType') === 'shelter' && <>
                        <label htmlFor="phone" className="mb-4">Teléfono</label>
                        <input type="text" className="b-input mt-4" name="phone" id="phone" ref={register({ required: true })}></input>
                    </>
                    }


                </div>

                <div className="row justify-content-center m-0 mb-5 mt-5">
                    <Link to="/myprofile" className="col-5 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark">Cancelar</Link>
                    <input type="submit" className="col-5 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light" value="Enviar" />
                </div>
            </form>

            {/* Modal de Enviado */}
            <div className="b-primereact-dialog">
                <Dialog visible={showModal} onHide={() => onHide()}>
                    <ModalSend signUp={signUp} saved={'edit'} redirect={'/myprofile'} />
                </Dialog>
            </div>
        </div>

        }
        </>

    )
}
