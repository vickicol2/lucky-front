import React, { useEffect, useContext, useState } from 'react';
import PhotoProfile from '../../../shared/components/PhotoProfile/PhotoProfile';
import { actualPageContext } from '../../../shared/contexts/actualPageContext';
import { Link } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { apiLucky } from '../../../shared/services/apiLucky';
import { useForm } from 'react-hook-form';
import ModalSend from '../../../shared/components/ModalSend/ModalSend';

const city = ['Madrid', 'Barcelona', 'Granada', 'Valencia', 'Sevilla'];

export default function MyProfile() {
    const [page, setPage] = useContext(actualPageContext);
    const { register, handleSubmit, errors } = useForm();
    const [profile, setProfile] = useState();

    useEffect(() => {
        setPage('other');

        // Quite el if principal del token
        if (localStorage.getItem('userType') === 'user') {
            apiLucky.get('/user/profile/')
                .then(res => {
                    if (res.data.message !== "error") { setProfile(res.data); }
                })
        } else if (localStorage.getItem('userType') === 'shelter') {
            apiLucky.get('/shelter/profile/' + localStorage.getItem('id'))
                .then(res => {
                    // console.log(res)
                    if (res.data.message !== "error") { setProfile(res.data.shelter); }
                })
        }
    }, []);




    return (
        <>
            {profile && <div className="c-my-profile d-flex flex-column align-items-center">
                <div className="b-margin__top--medium d-flex flex-row justify-content-center align-items-center">
                    <Link to="/profile" className="col-3 lucky-back-arrow b-color-salmon b-color-salmon--medium"></Link>
                    <PhotoProfile img={profile.img ? profile.img : '/img/profile.png'} />
                    <Link to="/myprofile/edit" className="ml-4 lucky-config b-color-salmon b-color-salmon--big"></Link>
                </div>

                <label className="mt-5 mb-3" htmlFor="fullname">Nombre completo: </label>
                <p className="mt-3 mb-4 text-center" id="fullname">{localStorage.getItem('userType') === 'user' ? profile.fullname : profile.name}</p>
                <div className="b-separator b-separator--small b-separator--salmon"></div>

                <label className="mt-5 mb-3" htmlFor="address">Dirección</label>
                <p className="mt-3 mb-4" id="address">{profile.address}</p>
                <div className="b-separator b-separator--small b-separator--salmon"></div>

                {localStorage.getItem('userType') === 'user' && <>
                    <label className="mt-5 mb-3" htmlFor="postalCode">Código Postal</label>
                    <p className="mt-3 mb-4" id="postalCode">{profile.postalCode}</p>
                    <div className="b-separator b-separator--small b-separator--salmon"></div>


                    <label className="mt-5 mb-3" htmlFor="city">Ciudad</label>
                    <p className="mt-3 mb-4" id="city">{profile.city}</p>
                    <div className="b-separator b-separator--small b-separator--salmon"></div>


                    <label className="mt-5 mb-3" htmlFor="dni">DNI</label>
                    <p className="mt-3 mb-4" id="dni">{profile.dni}</p>
                    <div className="b-separator b-separator--small b-separator--salmon"></div>
                </>
                }



                <label className="mt-5 mb-3" htmlFor="phone">Teléfono</label>
                <p className="mt-3 mb-4" id="phone">{profile.phone}</p>

            </div>

            }
        </>

    )
}
