import React, { useEffect, useContext, useState } from 'react';
import PhotoProfile from '../../shared/components/PhotoProfile/PhotoProfile';
import BtonArrow from '../../shared/components/BtnArrow/BtnArrow';
import Navbar from '../../shared/components/Navbar/Navbar';
import { actualPageContext } from '../../shared/contexts/actualPageContext';
import { apiLucky } from '../../shared/services/apiLucky';

export default function Profile() {
    const [page, setPage] = useContext(actualPageContext);
    const [profile, setProfile] = useState();

    useEffect(() => {
        setPage('profile');
        // Quite el if principal del token
        if (localStorage.getItem('userType') === 'user') {
            apiLucky.get('/user/profile/')
                .then(res => {
                    if (res.data.message !== "error") { setProfile(res.data); }
                })
        } else if (localStorage.getItem('userType') === 'shelter') {
            apiLucky.get('/shelter/profile/' + localStorage.getItem('id'))
                .then(res => {
                    // console.log(res)
                    if (res.data.message !== "error") { setProfile(res.data.shelter); }
                })
        }
    }, []);

    return (

        <div className="c-profile b-margin__bottom--extrabig">
            <div className="b-margin__top--medium d-flex flex-row justify-content-center">
                <PhotoProfile img={profile ? (profile.img ? profile.img : '/img/profile.png') : ''} />
            </div>

            <div className="b-margin__top--small">
                <BtonArrow icon={'lucky-boy'} text={'Mi perfil'} redirect={'/myprofile'} />
                <BtonArrow icon={'lucky-localization'} text={'Direcciones'} redirect={'/profile'} />
                {localStorage.getItem('userType') === 'user' && <BtonArrow icon={'lucky-heart'} text={'Favoritos'} redirect={'/profile'} />}
                <BtonArrow icon={'lucky-notification'} text={'Notificaciones'} redirect={'/profile'} />
            </div>

            {localStorage.getItem('userType') === 'user' && <div className="b-margin__top--big mb-5">
                <BtonArrow icon={'lucky-pet-adoption'} text={'Estado de la adopción'} redirect={'/state/list'} />
                <BtonArrow icon={'lucky-sponsor'} text={'Apadrinar'} redirect={'/profile'} />
                <BtonArrow icon={'lucky-donate'} text={'Donar'} redirect={'/profile'} />
            </div>

            }
        </div>
    )
}
