import React, { useState, useEffect, useContext } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";
import { Dialog } from 'primereact/dialog';
import BtnIcon from '../../shared/components/BtnIcon/BtnIcon';
import {
  GoogleMap,
  useLoadScript,
  InfoWindow,
  Marker,
} from "@react-google-maps/api";
import { apiLucky } from '../../shared/services/apiLucky';
import mapsServices from '../../shared/services/mapsServices.json';
import { actualPageContext } from '../../shared/contexts/actualPageContext';
import { Accordion, AccordionTab } from 'primereact/accordion';

const libraries = ["places"];
const mapContainerStyle = {
  height: "70vh",
  width: "100vw",
};

const options = {

  disableDefaultUI: true,
  zoomControl: true,
};

const center = {
  lat: 40.458926,
  lng: -3.704376,
};

const iconBase = 'http://maps.google.com/mapfiles/ms/icons/';

export default function Maps() {
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: process.env.MAPS_API_KEY,
    libraries,
  });



  const [page, setPage] = useContext(actualPageContext);
  const [markers, setMarkers] = useState([]);
  const [selected, setSelected] = useState(null);
  const [title, setTitle] = useState();
  const [lat, setLat] = useState();
  const [lng, setLng] = useState();
  const [img, setImg] = useState();
  const [address, setAddress] = useState();
  const [vet, setVet] = useState(true);
  const [vetSelected, setVetSelected] = useState(false);
  const [vetAll, setVetAll] = useState();
  const [creche, setCreche] = useState(true);
  const [crecheSelected, setCrecheSelected] = useState(false);
  const [crecheAll, setCrecheAll] = useState();
  const [shop, setShop] = useState(true);
  const [shopSelected, setShopSelected] = useState(false);
  const [shopAll, setShopAll] = useState();

  const [stateSelected, setStateSelected] = useState(false);
  const [btnSelected, setBtnSelected] = useState(false);
  const [itemsSelected, setItemsSelected] = useState([]);

  // Lista desplegable
  const [activeIndex, setActiveIndex] = useState(null);

  const onClickAcordion = (itemIndex) => {
    let activeIndex = activeIndex ? [...activeIndex] : [];

    if (activeIndex.length === 0) {
      activeIndex.push(itemIndex);
    }
    else {
      const index = activeIndex.indexOf(itemIndex);
      if (index === -1) {
        activeIndex.push(itemIndex);
      }
      else {
        activeIndex.splice(index, 1);
      }
    }

    setActiveIndex({ activeIndex });
  }

  // Modal
  const [showModal, setShowModal] = useState(false);
  const [position, setPosition] = useState('center');


  const onHide = () => {
    setShowModal(false);
    localStorage.getItem('MapsFilter') ? setItemsSelected(JSON.parse(localStorage.getItem('MapsFilter')).statePet) : setItemsSelected([]);

    const result = itemsSelected.filter(item => item.includes("Pet Friendly") || item.includes("Veterinario") || item.includes("Guarderia"));

    if (result.length > 0) {
      const vetSelected = result.findIndex(item => item === "Veterinario") >= 0 ? true : false;
      setVet(vetSelected);

      const crecheSelected = result.findIndex(item => item === "Guarderia") >= 0 ? true : false;
      setCreche(crecheSelected);

      const shopSelected = result.findIndex(item => item === "Pet Friendly") >= 0 ? true : false;
      setShop(shopSelected);
    } else {
      setVet(true);
      setCreche(true);
      setCreche(true);
    }

  };

  const openModalFilter = () => {
    setShowModal(true);
    if (position) {
      setPosition(position);
    }
  }

  const limpiarFilters = () => {
    mapsServices.map(item => {
      item.selected = false;
    })

    localStorage.removeItem('MapsFilter');
    setBtnSelected(false);
    setVetSelected(false);
    setShopSelected(false);
    setCrecheSelected(false);
    setVet(true);
    setCreche(true);
    setShop(true);
  }

  const bntStatePet = (item) => {
    setStateSelected(!stateSelected);

    mapsServices[item.id].selected = !mapsServices[item.id].selected;

    let arrStatePet = JSON.parse(localStorage.getItem('MapsFilter')) ? JSON.parse(localStorage.getItem('MapsFilter')).statePet : [];

    if (mapsServices[item.id].selected) {
      arrStatePet.push(item.name);
      if (item.name === "Veterinario") setVetSelected(true);
      if (item.name === "Pet Friendly") setShopSelected(true);
      if (item.name === "Guarderia") setCrecheSelected(true);
    } else {
      arrStatePet = arrStatePet.filter(value => value !== mapsServices[item.id].name);
      if (item.name === "Veterinario") setVetSelected(false);
      if (item.name === "Pet Friendly") setShopSelected(false);
      if (item.name === "Guarderia") setCrecheSelected(false);
    }

    localStorage.setItem('MapsFilter', JSON.stringify({ statePet: arrStatePet }));

    if (mapsServices.findIndex(i => i.selected) >= 0) {
      setBtnSelected(true);

    } else {
      setBtnSelected(false);
      localStorage.removeItem('MapsFilter');
    }
  }

  useEffect(() => {
    setPage('maps');

    apiLucky.get('/vet/')
      .then(res => {
        setVetAll(res.data.vet);
      })


    apiLucky.get('/creche/')
      .then(res => {
        setCrecheAll(res.data.creche);
      })


    apiLucky.get('/shop/')
      .then(res => {
        setShopAll(res.data.shop);
      })

      if (localStorage.getItem('MapsFilter')) {
        const resultState = JSON.parse(localStorage.getItem('MapsFilter')).statePet;

        resultState.map(specie => {
            mapsServices.map(item => {
                if (item.icon === specie) {
                    item.selected = true;
                }
            })
        })
    }

    if (mapsServices.findIndex(i => i.selected) >= 0) {
        setBtnSelected(true);
    } else {
        setBtnSelected(false);
        localStorage.removeItem('MapsFilter');
    }
  }, [])



  // ---- Maps

  const onMapClick = React.useCallback((e) => {
    setMarkers((current) => [
      ...current,
      {
        lat: e.latLng.lat(),
        lng: e.latLng.lng(),
        time: new Date(),
      },
    ]);
  }, []);

  const mapRef = React.useRef();
  const onMapLoad = React.useCallback((map) => {
    mapRef.current = map;
  }, []);

  const panTo = React.useCallback(({ lat, lng }) => {
    mapRef.current.panTo({ lat, lng });
    mapRef.current.setZoom(14);
  }, []);

  if (loadError) return "Error";
  if (!isLoaded) return "Loading...";

  const showVetDetail = (id) => {
    apiLucky.get('/vet/' + id)
      .then(res => {
        const result = res.data.vet;
        setTitle(result.name);
        setImg(result.img);
        setAddress(result.address);
        setLat(result.lat);
        setLng(result.lng);
        setSelected(true);
      })


  }

  const showCrecheDetail = (id) => {
    apiLucky.get('/creche/' + id)
      .then(res => {
        const result = res.data.creche;
        setTitle(result.name);
        setImg(result.img);
        setAddress(result.address);
        setLat(result.lat);
        setLng(result.lng);
        setSelected(true);
      });

    apiLucky.get('/creche/')
      .then(res => {
        console.log(res)
      })
  }

  const showShopDetail = (id) => {
    apiLucky.get('/shop/' + id)
      .then(res => {
        const result = res.data.shop;
        setTitle(result.name);
        setImg(result.img);
        setAddress(result.address);
        setLat(result.lat);
        setLng(result.lng);
        setSelected(true);
      })

    apiLucky.get('/shop/')
      .then(res => {
        console.log(res)
      })
  }

  return (
    <div className='c-maps'>
      <div className="row m-0 p-0 mt-5 mb-5 align-items-center justify-content-center">

        <div className='c-portait-adoption__status c-portait-adoption__status--process d-flex flex-row align-items-center mr-4'>
          <p className="mr-2">Veterinario</p>
          <span className="lucky-circle c-portait-adoption__span"></span>
        </div>


        <div className='c-portait-adoption__status c-portait-adoption__status--complete d-flex flex-row align-items-center mr-4'>
          <p className="mr-2">Pet Friendly</p>
          <span className="lucky-circle c-portait-adoption__span"></span>
        </div>


        <div className='c-portait-adoption__status c-portait-adoption__status--available d-flex flex-row align-items-center'>
          <p className="mr-2">Guardería</p>
          <span className="lucky-circle c-portait-adoption__span"></span>
        </div>


        <button className="col-1 col-md-2 p-0 b-color-salmon b-color-salmon--big ml-2" onClick={() => { openModalFilter() }}><span className="lucky-filter"></span></button>
      </div>

      <GoogleMap
        id="map"
        mapContainerStyle={mapContainerStyle}
        zoom={8}
        center={center}
        options={options}
        onClick={onMapClick}
        onLoad={onMapLoad}
      >
        {vet && <>
          <Marker position={{ lat: 40.443146, lng: -3.699453 }} icon={iconBase + "yellow-dot.png"} onClick={() => showVetDetail('5f46b8120cc2555bf48ae6f0')} />
          <Marker position={{ lat: 40.43407, lng: -3.698575 }} icon={iconBase + "yellow-dot.png"} onClick={() => showVetDetail('5f46b2660cc2555bf48ae6ed')} />
          <Marker position={{ lat: 40.458926, lng: -3.704376 }} icon={iconBase + "yellow-dot.png"} onClick={() => showVetDetail('5f46b8e00cc2555bf48ae6f1')} />

        </>
        }

        {creche && <>
          <Marker position={{ lat: 40.436546, lng: -3.712172 }} icon={iconBase + "blue-dot.png"} onClick={() => showCrecheDetail('5f46b4820cc2555bf48ae6ee')} />
          <Marker position={{ lat: 40.513516, lng: -3.925058 }} icon={iconBase + "blue-dot.png"} onClick={() => showCrecheDetail('5f46ba8c0cc2555bf48ae6f2')} />
        </>
        }


        {shop && <>
          <Marker position={{ lat: 40.430474, lng: -3.711668 }} icon={iconBase + "green-dot.png"} onClick={() => showShopDetail('5f46b63f0cc2555bf48ae6ef')} />
          <Marker position={{ lat: 40.459867, lng: -3.699307 }} icon={iconBase + "green-dot.png"} onClick={() => showShopDetail('5f46bc410cc2555bf48ae6f3')} />
        </>
        }


        {selected ? (
          <InfoWindow
            position={{ lat, lng }}
            onCloseClick={() => {
              setSelected(null);
            }}
          >

            <div className="d-flex flex-column align-items-center">
              <h4 className="mb-3">{title}</h4>
              <p className="mb-3">{address}</p>
              <img src={img} alt={title}></img>

            </div>
          </InfoWindow>
        ) : null}
      </GoogleMap>


      {vetSelected && vetAll && <Accordion activeIndex={0} className="b-primereact-acordion b-margin__top--medium b-margin__bottom--extrabig">
        {vetAll.map((item, index) => {
          return <AccordionTab key={index} header={item.name}>
          <div className="b-primereact-acordion__img">
            <img src={item.img} alt={item.name} />
          </div>
            <div className="d-flex flex-column justify-content-center">
              <h4 className="mb-4">{item.name}</h4>
              <p>{item.address}</p>
            </div>
          </AccordionTab>
        })
        }
      </Accordion>
      }

      {shopSelected && shopAll && <Accordion activeIndex={0} className="b-primereact-acordion b-margin__top--medium b-margin__bottom--extrabig">
        {shopAll.map((item, index) => {
          return <AccordionTab key={index} header={item.name}>
          <div className="b-primereact-acordion__img">
            <img src={item.img} alt={item.name} />
          </div>
            <div className="d-flex flex-column justify-content-center">
              <h4 className="mb-4">{item.name}</h4>
              <p>{item.address}</p>
            </div>
          </AccordionTab>
        })
        }
      </Accordion>
      }


      {crecheSelected && crecheAll && <Accordion activeIndex={0} className="b-primereact-acordion b-margin__top--medium b-margin__bottom--extrabig">
        {crecheAll.map((item, index) => {
          return <AccordionTab key={index} header={item.name}>
          <div className="b-primereact-acordion__img">
            <img src={item.img} alt={item.name} />
          </div>
            <div className="d-flex flex-column justify-content-center">
              <h4 className="mb-4">{item.name}</h4>
              <p>{item.address}</p>
            </div>
          </AccordionTab>
        })
        }
      </Accordion>
      }

      {/* Modal */}
      <div className="b-primereact-dialog">
        <Dialog visible={showModal} onHide={() => onHide()}>
          <div className="b-box-filter">
            <div className="c-filter__buttons d-flex flex-row flex-wrap" id="animalBtns">
              {
                mapsServices.map((item, index) => {
                  return <BtnIcon key={index} onClick={() => bntStatePet(item)} selected={stateSelected} item={item} />
                })
              }
            </div>

            {/* Muestra el botón de aplicar si no hay elementos (btonIcons) seleccionados */}
            {!btnSelected && <div className="row justify-content-center m-0 mb-5">
              <button className="col-11 b-btn-rectangle b-btn-rectangle--medium b-btn-rectangle--salmon-dark" onClick={() => onHide()} >Aplicar</button>
            </div>
            }

            {/* Muestra el botón de aplicar y borrar cuando hay por lo menos un elemento (btonIcons) seleccionado */}
            {btnSelected && <div className="row justify-content-center m-0 mb-5">
              <button className="col-5 mr-4 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-light" onClick={() => limpiarFilters()}>Borrar filtros</button>
              <button className="col-5 b-btn-rectangle b-btn-rectangle--small b-btn-rectangle--salmon-dark" onClick={() => onHide()}>Aplicar</button>
            </div>
            }
          </div>
        </Dialog>
      </div>
    </div>
  );
}

