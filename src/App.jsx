import React, { useState } from 'react';
import './App.scss';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";
import OnBoarding from './pages/OnBoarding/OnBoarding';
import Getin from './pages/GetIn/GetIn';
import Login from './pages/GetIn/Login/Login';
import Register from './pages/GetIn/Register/Register';
import Home from './pages/Home/Home';
import Adoption from './pages/Adoptions/Adoptions';
import FormAdoption from './pages/Adoptions/FormAdoptions/FormAdoptions';
import Filters from './pages/Adoptions/Filters/Filters';
import Pets from './pages/Adoptions/Pets/Pets';
import PetsDetails from './pages/Adoptions/Pets/PetsDetails/PetsDetails';
import PetAdoption from './pages/Adoptions/Pets/PetAdoption/PetAdoption';
import Profile from './pages/Profile/Profile';
import Plus from './pages/Plus/Plus';
import StatusList from './pages/Adoptions/StatusList/StatusList';
import StatusDetails from './pages/Adoptions/StatusList/StatusDetails/StatusDetails';
import StatusFilter from './pages/Adoptions/StatusList/StatusFilter/StatusFilter';
import PetsRegister from './pages/PetsRegister/PetsRegister';
import Navbar from './shared/components/Navbar/Navbar';
import Maps from './pages/Maps/Maps';
import MyProfile from './pages/Profile/MyProfile/MyProfile';
import { isLoggedContext } from './shared/contexts/isLoggedContext';
import { actualPageContext } from './shared/contexts/actualPageContext';
import PrivateRoute from './shared/components/PrivateRoute/PrivateRoute';
import FormEdit from './pages/Profile/MyProfile/FormEdit/FormEdit';


export default function App() {
  const [page, setPage] = useState([]);  // Guarda la pagina actual y permite cambiar los iconos del navbar
  const [isLogged, setIsLogged] = useState(!!localStorage.getItem('token')); // !! permite convertir un dato en booleano


  return (
    <Router>
      <actualPageContext.Provider value={[page, setPage]}>
        <isLoggedContext.Provider value={[isLogged, setIsLogged]}>
          <Switch>

            <Route path="/getin">
              <Getin />
            </Route>

            <Route path="/login">
              <Login />
            </Route>

            <Route path="/register/user">
              <Register />
            </Route>

            <PrivateRoute path="/home">
              <Home />
            </PrivateRoute>

            <PrivateRoute path="/maps">
              <Maps />
            </PrivateRoute>

            <PrivateRoute path="/adoption/request">
              <FormAdoption />
            </PrivateRoute>

            <PrivateRoute path="/adoption/pet/:id">
              <PetAdoption />
            </PrivateRoute>

            <PrivateRoute path="/adoption">
              <Adoption />
            </PrivateRoute>

            <PrivateRoute path="/register/pet">
              <PetsRegister/>
            </PrivateRoute>

            <PrivateRoute path="/state/details/:id">
              <StatusDetails />
            </PrivateRoute>

            <PrivateRoute path="/state/list">
              <StatusList />
            </PrivateRoute>

            <PrivateRoute path="/state/filter">
              <StatusFilter />
            </PrivateRoute>


            <PrivateRoute path="/filterpet/result">
              <Pets />
            </PrivateRoute>

            <PrivateRoute path="/filterpet">
              <Filters />
            </PrivateRoute>

            <PrivateRoute path="/mypet/:id">
              <PetsDetails />
            </PrivateRoute>

            <PrivateRoute path="/profile">
              <Profile />
            </PrivateRoute>

            <PrivateRoute path="/myprofile/edit">
              <FormEdit />
            </PrivateRoute>

            <PrivateRoute path="/myprofile">
              <MyProfile />
            </PrivateRoute>

            

            <PrivateRoute path="/plus">
              <Plus />
            </PrivateRoute>

            <Route path="/">
              <OnBoarding />
            </Route>

          </Switch>

          <Navbar/>
        </isLoggedContext.Provider>
      </actualPageContext.Provider>
    </Router>
  );
}


