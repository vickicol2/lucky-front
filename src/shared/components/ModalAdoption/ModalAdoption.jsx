import React from 'react';
import './ModalAdoption.scss';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";

export default function ModalAdoption(props) {
    return (
        <div className="c-modal-adoption">
            <h4 className="c-modal-adoption__title">Solicitud de adopción</h4>
            <p className="c-modal-adoption__p">Adoptar es un acto de amor, pero sobre todo una responsabilidad de por vida</p>
            <p className="c-modal-adoption__p">Por éste motivo es importante que veas el siguiente vídeo donde te explicamos como va a ser todo el proceso de adopción</p>
            <div className="c-modal-adoption__img">
                <img src="/img/request.png" alt="Solicitud de adopción" />
            </div>
            <h4 className="c-modal-adoption__subtitle">¿Quieres continuar con el proceso de adopción?</h4>
            <div className="c-modal-adoption__content-links">
                <button className="c-modal-adoption__link c-modal-adoption__link--cancel" onClick={props.onClick}>Cancelar</button>
                <Link className="c-modal-adoption__link c-modal-adoption__link--next" to="/adoption/request">Continuar</Link>
            </div>
        </div>
    )
}
