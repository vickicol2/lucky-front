import React from 'react';
import './PortaitAdoption.scss';
import { Link } from 'react-router-dom';

export default function PortaitAdoption(props) {

    const petsAdoptions = props.petsAdoptions;
    const type = props.type;

    return (
        <>
            { petsAdoptions && type==="list" && petsAdoptions.map((item, index) => {
                return <div key={index} className="c-portait-adoption d-flex flex-column">

                    <div className="c-portait-adoption__header mb-5 mt-4 d-flex flex-row align-items-center">
                        <h4 className="c-portait-adoption__title">Adopción de {item.name}</h4>

                        {item.statePet === "En proceso" && <div className='c-portait-adoption__status c-portait-adoption__status--process d-flex flex-row align-items-center'>
                            <p className="mr-3">{item.statePet}</p>
                            <span className="lucky-circle c-portait-adoption__span"></span>
                        </div>
                        }

                        {item.statePet === "Aceptado" && <div className='c-portait-adoption__status c-portait-adoption__status--complete d-flex flex-row align-items-center'>
                            <p className="mr-3">{item.statePet}</p>
                            <span className="lucky-circle c-portait-adoption__span"></span>
                        </div>
                        }

                        {item.statePet === "Rechazado" && <div className='c-portait-adoption__status c-portait-adoption__status--reject d-flex flex-row align-items-center'>
                            <p className="mr-3">{item.statePet}</p>
                            <span className="lucky-circle c-portait-adoption__span"></span>
                        </div>
                        }

                        {item.statePet === "Disponible" && <div className='c-portait-adoption__status c-portait-adoption__status--available d-flex flex-row align-items-center'>
                            <p className="mr-3">{item.statePet}</p>
                            <span className="lucky-circle c-portait-adoption__span"></span>
                        </div>
                        }

                    </div>
                    <div className="c-portait-adoption__body mb-4 d-flex flex-row align-items-center">
                        <Link to={"/state/details/" + item._id} className="c-portait-adoption__content-img">
                            <img className="c-portait-adoption__img" src={item.img ? item.img : '/img/petsDefault.jpg'} alt="Solicitud de adopción" />
                        </Link>
                        <div className="c-portait-adoption__content-data-title">
                            <p className="mb-3">Nombre</p>
                            <p className="mb-3">Ciudad</p>
                            <p className="mb-3">Sexo</p>
                        </div>
                        <div className="c-portait-adoption__content-data">
                            <p className="mb-3">{item.name}</p>
                            <p className="mb-3">{item.city}</p>
                            <p className="mb-3">{item.gender}</p>
                        </div>
                    </div>

                </div>
            })
            }

            { petsAdoptions && type==="details" && 
                 <div className="c-portait-adoption d-flex flex-column">

                    <div className="c-portait-adoption__header mb-5 mt-4 d-flex flex-row align-items-center">
                        <h4 className="c-portait-adoption__title">Adopción de {petsAdoptions.name}</h4>
                    </div>

                    <div className="c-portait-adoption__body mb-4 d-flex flex-row align-items-center">
                        <div className="c-portait-adoption__content-img">
                            <img className="c-portait-adoption__img" src={petsAdoptions.img ? petsAdoptions.img : '/img/petsDefault.jpg'} alt="Solicitud de adopción" />
                        </div>
                        <div className="c-portait-adoption__content-data-title">
                            <p className="mb-3">Nombre</p>
                            <p className="mb-3">Ciudad</p>
                            <p className="mb-3">Sexo</p>
                        </div>
                        <div className="c-portait-adoption__content-data">
                            <p className="mb-3">{petsAdoptions.name}</p>
                            <p className="mb-3">{petsAdoptions.city}</p>
                            <p className="mb-3">{petsAdoptions.gender}</p>
                        </div>
                    </div>

                </div>
            

            }
        </>

    )
}
