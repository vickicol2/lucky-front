import React from 'react';

export default function CheckBox(props) {
    const data = props.data;

    return (
        <label className="b-margin__top--small d-flex flex-row justify-content-between">
            <span>{data.title}</span>
            <div className="d-flex flex-row justify-content-end">
                <div>
                    <input type="radio" className="ml-5 b-color-pineGreen" name={data.id} value={data.value[0]} ref={data.ref} />
                    <span className="b-color-pineGreen">{data.name[0]} </span>
                </div>
                <div>
                    <input type="radio" className="ml-5 b-color-pineGreen" name={data.id} value={data.value[1]} ref={data.ref} />
                    <span className="b-color-pineGreen">{data.name[1]}</span>
                </div>
            </div>
        </label>
    )
}
