import React from 'react';
import './MenuHome.scss';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";

export default function MenuHome(props) {
    return (
        <Link className="c-menu-home" to={props.contenidoSlide.redirect}>
            <span className={props.contenidoSlide.icon}></span>
            <div className="d-flex flex-column ml-2">
                <h4 className="d-flex">{props.contenidoSlide.title}</h4>
                <p className="c-menu-home__p">{props.contenidoSlide.body}</p>
            </div>
        </Link>
    )
}
