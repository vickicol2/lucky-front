import React from 'react';
import './InputSearch.scss';

export default function InputSearch(props) {
    const nameClass = `c-input-search c-input-search ${props.tamaño}`;

    return (
        <div className={nameClass}>
            <input type="search" className="c-input-search__text" placeholder="Buscar"/>
            <span className="lucky-search c-input-search__icon"></span>
        </div>
    )
}
