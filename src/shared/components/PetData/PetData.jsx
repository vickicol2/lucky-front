import React from 'react';

export default function PetData(props) {
    return (
        <div className="c-pet-data mb-4 pb-3 col-12 d-flex flex-row justify-content-between">
            <div className="d-flex flex-row align-items-center">
                <span className="lucky-paw b-color-ocean b-color-ocean--big mr-3"></span>
                <p>{props.title}</p>
            </div>
            <p className="mr-2">{props.content}</p>
        </div>
    )
}
