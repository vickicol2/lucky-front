import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";

export default function LinkIcon(props) {

    return (
        <Link className="b-btn-icon b-btn-icon--ocean-dark" to={'/mypet/' + props.petsAdoptions.id}>
            <span className={"b-btn-icon__span c-btn-icon--big lucky-" + props.petsAdoptions.specie}></span>
            <p>{props.petsAdoptions.name}</p>
        </Link>

    )
}
