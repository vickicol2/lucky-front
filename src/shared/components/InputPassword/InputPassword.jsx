import React from 'react';
import './InputPassword.scss';

export default function InputPassword() {
    return (
        <div className="c-input-password">
            <input type="password" className="c-input-password__text"/>
            <span className="lucky-eye c-input-password__icon"></span>
            <p className="c-input-password__p">¿Has olvidado tu contraseña?</p>
        </div>
    )
}
