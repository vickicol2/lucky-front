import React from 'react';
import './Slide.scss';
import { Link } from "react-router-dom";
import { Carousel } from 'primereact/carousel';



const contenidoSlide = [
    {
        title: 'Encuentra todo tipo de servicios que tienes cerca de ti',
        body: '',
        src: "/img/onboarding1.png",
        alt: "Sitios"
    },
    {
        title: 'Adopta desde tu móvil',
        body: 'Puedes acceder al perfil de muchos animales que están en adopción y filtrarlos para encontrar el que mejor se adapte a ti',
        src: "/img/onboarding2.png",
        alt: "Adoptar"
    },
    {
        title: 'Si eres una asociación sube a tus peludos para darles más difusión',
        body: '',
        src: "/img/onboarding3.png",
        alt: "Asociación Protectora"
    },
]

export default function Slide() {

    const templateSlide = (contenidoSlide) => {
        return <div className="c-slide__content">
            {/* <Link to="/getin"><span className="lucky-close"></span></Link> */}

            <div className="c-slide__img">
                <img src={contenidoSlide.src} alt={contenidoSlide.alt} />
            </div>

            <h4 className="c-slide__title">{contenidoSlide.title}</h4>
            {contenidoSlide.body !== '' && <p className="c-slide__p">{contenidoSlide.body}</p>}

        </div>
    }

    return (
        <div className="c-slide">
            <Link className="c-slide__link mt-5 mr-5 mb-5" to="/getin"><span className="lucky-close"></span></Link>
            <div className="b-primereact-carousel mt-5 pt-5">
                <Carousel className="pt-5" value={contenidoSlide} itemTemplate={templateSlide} numVisible={1} numScroll={1} circular={true}></Carousel>
            </div>
        </div>
    )
}






