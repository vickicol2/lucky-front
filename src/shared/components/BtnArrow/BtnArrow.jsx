import React from 'react';
import './BtnArrow.scss';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";

export default function BtonArrow(props) {
    const nameClass = `${props.icon} mr-4 c-btn-arrow__icon`;

    return (
        <Link to={props.redirect} className="c-btn-arrow">
            <div  className="c-btn-arrow__content">
                {props.icon !== '' && <span className={nameClass}></span>}
                <p>{props.text}</p>
            </div>
            <span className="lucky-next-arrow b-color-salmon mr-4"></span>
        </Link>
    )
}
