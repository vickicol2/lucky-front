import React, { useState, useEffect, useContext } from 'react';
import './BoxFilter.scss';
import BtnIcon from '../BtnIcon/BtnIcon';
import statusServices from '../../services/statusServices.json';

export default function BoxFilter() {
    const [btn_apply_clear_status, setBtn_apply_clear_Status] = useState(false);

    useEffect(() => {
        localStorage.getItem('statusFilter') ? setBtn_apply_clear_Status(true) : setBtn_apply_clear_Status(false);
    }, [])

    const limpiarFilters = () => {
        statusServices.map(item => {
            item.selected = false;
        })

        localStorage.removeItem('statusFilter');
        setBtn_apply_clear_Status(false);
    }


    return (
            <div className="c-box-filter">
                <h2>Filtros</h2>
                <div className="c-box-filter__buttons">
                    {
                        statusServices.map((item, index) => {
                            return <BtnIcon key={index} item={item} type={'status'} existsLocal={btn_apply_clear_status} />
                        })
                    }
                </div>
                <button className="c-box-filter__btn b-btn-rectangle b-btn-rectangle--medium b-btn-rectangle--salmon-dark">Aceptar</button>
            </div>
    )
}
