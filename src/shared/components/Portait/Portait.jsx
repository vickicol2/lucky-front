import React from 'react';
import './Portait.scss';
import { Link } from 'react-router-dom';

export default function Portait(props) {
    const contentType = props.contentType;

    return (
        <>
            {props.contentBody ?
                props.contentBody.map((item, index) => {
                    return <div key={index} className={contentType === "news" ? "c-portait c-portait--salmon-dark mb-4" : (contentType === "pets" ? "c-portait c-portait--ocean-light" : '')}>
                        {/* Novedades */}
                        {contentType === "news" && <>
                            <div className="c-portait__content-img"><img className="c-portait__img" src={item.src} alt={item.title} /></div>
                            <Link to={'/home' + item.id} className="c-portait__title--light c-portait__title--news">{item.title}</Link> </>
                        }


                        {/* Pets */}
                        {contentType === "pets" && <>
                            <div className="c-portait__content-img"><img className="c-portait__img" src={item.img?  item.img: '/img/petsDefault.jpg'} alt={item.name} /></div>

                            <div className="c-portait__content-pets">
                                <button className="c-portait__favorite">
                                    <span className="lucky-like"></span>
                                </button>
                                
                                <Link to={localStorage.getItem('userType') === 'shelter' ? '/mypet/' + item.id : '/adoption/pet/' + item.id} className="c-portait__title--animals">{item.name}</Link>
                                <p className="c-portait__p mr-5">{item.city}</p>

                            </div> </>
                        }
                    </div>
                }) : ''
            }
        </>
    )
}
