import React, { useEffect, useContext, useState } from 'react';
import './Navbar.scss';
import { Link } from "react-router-dom";
import { actualPageContext } from '../../contexts/actualPageContext';
import { apiLucky } from '../../../shared/services/apiLucky';
import PhotoProfile from '../../../shared/components/PhotoProfile/PhotoProfile';
import { isLoggedContext } from '../../../shared/contexts/isLoggedContext';

export default function Navbar() {
    const [page, setPage] = useContext(actualPageContext);
    const [isLogged, setIsLogged] = useContext(isLoggedContext);
    const [profile, setProfile] = useState();

    useEffect(() => {
        // Quite un if principal de if localstorage token existe (engloba los dos if de abajo)

        if (localStorage.getItem('userType') === 'user') {
            apiLucky.get('/user/profile/')
                .then(res => {
                    if (res.data.message !== "error") { setProfile(res.data); }
                })

        } else if (localStorage.getItem('userType') === 'shelter') {
            apiLucky.get('/shelter/profile/' + localStorage.getItem('id'))
                .then(res => {
                    if (res.data.message !== "error") { setProfile(res.data.shelter); }
                })
        }
    }, [isLogged]);

    return (
        <>
            {page !== 'other' && <div className="c-navbar">
                <Link className={page === 'home' ? "c-navbar__link c-navbar__link--active" : "c-navbar__link c-navbar__link--inactive"} to="/home">
                    <span className="lucky-home"></span>
                </Link>
                <Link className={page === 'maps' ? "c-navbar__link c-navbar__link--active" : "c-navbar__link c-navbar__link--inactive"} to="/maps">
                    <span className="lucky-maps"></span>
                </Link>
                <Link className={page === 'adoption' ? "c-navbar__link c-navbar__link--active" : "c-navbar__link c-navbar__link--inactive"} to="/adoption">
                    <span className="lucky-pet-adoption"></span>
                </Link>
                <Link className={page === 'profile' ? "c-navbar__link c-navbar__link--active" : "c-navbar__link c-navbar__link--inactive"} to="/profile">
                    <PhotoProfile type={'navbar'} img={profile ? (profile.img ? profile.img : '/img/profile.png') : ''} />
                </Link>
                <Link className={page === 'plus' ? "c-navbar__link c-navbar__link--active" : "c-navbar__link c-navbar__link--inactive"} to="/plus">
                    <span className="lucky-mas"></span>
                </Link>
            </div>

            }
        </>

    )
}
