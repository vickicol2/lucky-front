import React from 'react';
import './ModalSend.scss';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";


export default function ModalSend(props) {
    return (
        <div className="c-modal-send">

            {props.signUp && props.saved === 'pet' && <>
                <Link className="lucky-close c-modal-send__close" to="/adoption"></Link>
                <p className="c-modal-send__p">Tu mascota ha sido registrada exitosamente</p>
                <p className="c-modal-send__p">¡Puedes revisar sus datos!</p>
                <div className="c-modal-send__img">
                    <img src="/img/send.png" alt="Enviado" />
                </div>
            </>
            }

            {props.signUp && props.saved === 'adoption' && <>
                <Link className="lucky-close c-modal-send__close" to="/adoption"></Link>
                <h4>¡Enviado!</h4>
                <p className="c-modal-send__p">Hemos enviado tu formulario a la protectora. Si quieres ponerte en contacto con ellos puedes hacerlo vía email o whatsapp</p>
                <p className="c-modal-send__p">Recuerda que la protectora se pondrá en contacto contigo para poder hacer la entrevista personal</p>
                <div className="c-modal-send__img">
                    <img src="/img/send.png" alt="Enviado" />
                </div>
            </>
            }

            {props.signUp && props.saved === 'edit' && <>
                <Link className="lucky-close c-modal-send__close" to={props.redirect}></Link>
                <p className="c-modal-send__p">Tus datos han sido modificados con éxito</p>
                <div className="c-modal-send__img">
                    <img src="/img/send.png" alt="Enviado" />
                </div>
            </>
            }


            {props.signUp && (props.saved === 'user' || props.saved === 'shelter') && <>
                <Link className="lucky-close c-modal-send__close" to={props.redirect}></Link>
                <p className="c-modal-send__p">Te has registrado exitosamente</p>
                <p className="c-modal-send__p">¡Puedes iniciar sesión!</p>
                <div className="c-modal-send__img">
                    <img src="/img/send.png" alt="Enviado" />
                </div>
            </>
            }




            {!props.signUp && <>
                <Link className="lucky-close c-modal-send__close" to={props.saved === 'pet' ? "/adoption" : (props.saved === 'adoption' ? "/status/list" : (props.save === 'edit' ? "/profile" : "login"))}></Link>
                <p className="c-modal-send__p">Lo siento. Ha ocurrido un error</p>
                <p className="c-modal-send__p">¡Vuelve a intentarlo!</p>
                <div className="c-modal-send__img">
                    <img src="/img/send.png" alt="Enviado" />
                </div>
            </>
            }



        </div>
    )
}
