import React from 'react';
import specieServices from '../../services/specieServices.json';
import genderServices from '../../services/genderServices.json';
import sizeServices from '../../services/sizeServices.json';

let btns_select = [-1, -1, -1];

export default function BtnIcon(props) {
    const item = props.item;
    const selected = props.selected;  // specie, gender o size
    const existsLocal = props.existsLocal;
    
    return (
        <button onClick={props.onClick} className={item.selected ? "b-btn-icon b-btn-icon--salmon-light" : "b-btn-icon b-btn-icon--ocean-light"}>
            <span className={"b-btn-icon__span lucky-" + item.icon + (item.name === 'Pequeño' ? " b-btn-icon--small" : (item.name === 'Mediano' ? " c-btn-icon--medium" : " c-btn-icon--big"))}></span>
            <p>{item.name}</p>
        </button>
    )
}
