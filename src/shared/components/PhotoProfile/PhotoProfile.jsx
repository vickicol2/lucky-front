import React from 'react';
import './PhotoProfile.scss';

export default function PhotoProfile(props) {

    return (
        <div className={props.type === 'navbar' ? "c-photo-profile c-photo-profile--small" : "c-photo-profile c-photo-profile--big"}>
            <img className="c-photo-profile__img" src={props.img} alt="User"/>
        </div>
    )
}
