import axios from 'axios';

export const apiLucky = axios.create({
    baseURL: process.env.REACT_APP_BACK_URL,
    timeout: 100000,
    headers: {
        'Accept': 'application/json; multipart/form-data',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
    }
});

